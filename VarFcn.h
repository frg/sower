#ifndef _VAR_FCN_H_
#define _VAR_FCN_H_

#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <Vector3D.h>

//------------------------------------------------------------------------------

class VarFcn {

public:

  const char** pname;

protected:

  double gam;
  double gam1;
  double invgam1;

public:

  VarFcn(double g) { gam = g; gam1 = gam - 1.0; invgam1 = 1.0 / gam1; }
  ~VarFcn() {}

  double getGamma() const { return gam; }

  virtual void conservativeToPrimitive(double *, double *) = 0;
  virtual void primitiveToConservative(double *, double *) = 0;
  virtual void multiplyBydVdU(double *, double *, double *) = 0;
  virtual void preMultiplyBydUdV(double *, double *, double *) = 0;
  virtual void postMultiplyBydVdU(double *, double *, double *) = 0;
  virtual void postMultiplyBydUdV(double *, double *, double *) = 0;

  virtual Vec3D getVelocity(double *V) { return Vec3D(V[1], V[2], V[3]); }
  virtual double getDensity(double *V) { return V[0]; }
  virtual double getPressure(double *V) { return V[4]; }
  virtual double getTurbulentKineticEnergy(double *V) { return 0.0; }
  virtual double getTurbulentDissipationRate(double *V) { return 0.0; }
  virtual double computeTemperature(double *V) { return invgam1 * V[4] / V[0]; }
  virtual double computeMachNumber(double *V) {
    return sqrt((V[1]*V[1] + V[2]*V[2] + V[3]*V[3]) * V[0] / (gam * V[4]));
  }
  virtual double computeSoundSpeed(double *V) { return sqrt(gam * V[4] / V[0]); }
  virtual double computeCp(double *V, double mach) { return 2.0 * (V[4] - 1.0/(gam*mach*mach)); }

};

//------------------------------------------------------------------------------

#endif
