#ifndef _POSTPRO_H_
#define _POSTPRO_H_

#include <PostOperator.h>
#include <BinFileHandler.h>
#include <cstdio>

template<class Scalar> class Vec;

//------------------------------------------------------------------------------

struct FileNames {

  bool ascii;
  bool binary;
  bool ensight;
  bool isSdesign;
  const char *prefix;
  const char *global;
  const char *geo;
  const char *match;
  const char *dec;
  const char *result;
  const char *dens;
  const char *pres;
  const char *temp;
  const char *k;
  const char *eps;
  const char *mach;
  const char *vel;
  const char *disp;
  const char *skin;
  const char *nodemap;
  const char *name;
  const char *load;
  const char *nodes;
  const char *elements;
  const char *topo;

public:

  FileNames() 
  { 
    ascii = false;
    binary = false;
    ensight = false;
    isSdesign = false;
    prefix = ""; 
    global = "";
    geo = "";
    match = "";
    dec = "";
    result = "";
    dens = "";
    pres = "";
    temp = "";
    k = "";
    eps = "";
    mach = "";
    vel = "";
    disp = "";
    skin = "";
    nodemap = "";
    name = "";
    load = "load";
    nodes = "";
    elements = "";
    topo = "topo.xpost";
  }
  ~FileNames() {}

};

//------------------------------------------------------------------------------

struct PostOpts {

  int freqResPicks;
  int numResPicks;
  int (*resPicks)[2];

  int numBCs;
  int *bcs;

  double machInf;

public:

  PostOpts()
  {
    freqResPicks = 1;
    numResPicks = 0;
    resPicks = new int[100][2];

    numBCs = 5;
    bcs = new int[100];
    bcs[0] = -5;
    bcs[1] = -4;
    bcs[2] = -3;
    bcs[3] = -2;
    bcs[4] = -1;

    machInf = -1.0;
  }
  ~PostOpts() {}

  int tagResults(int, int *&);

};

//------------------------------------------------------------------------------

class FluidPostOp : public PostOp {

  const char *nameGeo;

  int numClusters;
  int numNodes;
  int numTets;
  int numFaces;
  
  int *numClusNodes;
  int *numClusTets;
  int *numClusFaces;

  int **nodeMap;

  double (*nodes)[3];
  int (*tets)[4];
  int *tetVolumeID;
  int (*faces)[4];
  int *facesSurfaceID;

  Vec<double> *ushared;
  Vec<double> **udist;
  Vec<int> **udistint;

  int numGlobSub;
  int (*subToCluster)[2];

  bool quiet;

public:

  enum TypeFile {SHARED = 0, DISTRIBUTED = 1};

public:
  
  FluidPostOp();
  ~FluidPostOp();

  void initialize(const char *, const char *, int = 0);
  void setQuietMode(bool);

  int getNumberOfNodes();
  int getNumberOfElements();
  int getNumberOfFaces();

  double (*getNodes())[3];
  int (*getElements())[4];
  int (*getFaces())[4];

  void releaseNodes();
  void releaseElements();
  void releaseFaces();

  int getNumberOfSolutions(const char *);
  int getNumberOfNodalSolutions(const char *);
  bool getSolution(const char *, int, TypeSol, double &, double *);

  void readSolutionHeader(const char *, int *, TypeFile);
  void writeSolutionHeader(const char *, int *, TypeFile);
  void readSolution(const char *, int, int, double &, double *, TypeFile);
  void readSolution(const char *, int, int, double &, int *, TypeFile);
  void writeSolution(const char *, int, int, double, double *, TypeFile);
  void writeSolution(const char *, int, int, double, int *, TypeFile);
  void writeSolutionSdesign(const char *, int, int *, int, int, double, double *, TypeFile);
  void writeSolutionSdesign(const char *, int, int *, int, int, double, int *, TypeFile);
  void writeSolutionEnsight(const char *, int, int, double, double *, TypeFile);
  int  extractSkinSurface(FileNames &, PostOpts &, int *&);
  void writeTopology(FileNames&, int, int, int);
  int getRangeInfo(BinFileHandler&, int&, int (*&)[2]);
  void writeDecomp(FileNames&);

  bool isPosition;

};

//------------------------------------------------------------------------------

class StructPostOp { //: public PostOp {

  const char *nameGeo;
  const char *nameRes;

  int numRes;
  int dim;
  int headerSize;
  int numClusters;
  int numItems;

  int *numClusItems;

  int **clusToGlMap;
  // int **locToClusMap;
  int **locToGlMap;
  int *nClusData;
  BinFileHandler::OffType *soltnStart;

  int *elemTypeNumNodesMap;  // for determining numNodes in an element
  int *glElemLengths;
  int *clusLengths;
  int *glElemOffsets;

public:

  StructPostOp();
  ~StructPostOp() {};

  void readClusterToSub(const char *);
  char *readSolutionHeader(const char *, BinFileHandler::OffType &, int &);
  void initialize(/*char *,*/ const char *, int, BinFileHandler::OffType &);
  void processNodeSoltns(FILE *&, int, int *, int, int);
  void processElemSoltns(FILE *&, int, int *, int, int);
  //void writeSolutionHeader(char *, int *, TypeFile);
  void writeXpostHeader(const char *, const char *, FILE *&, int);
  void writeXpostNodeSolution(double tag, double *data, FILE *&fp, int, int, int);
  void writeXpostElemSolution(double tag, double *data, FILE *&fp, int, int, int);
  void readNodes(BinFileHandler &, int);
  void readElems(BinFileHandler &, int, int *);
  void setElemTypeMap();

  int getNumClus()  { return numClusters; }
  int getNumRes()   { return numRes; }
};

//------------------------------------------------------------------------------

class XpostOp {

  int size;
  int current;
  const char* fname;
  FILE* fp;

public:

  bool isSdesign;
  XpostOp(const char*);
  ~XpostOp();

  void readSolutionHeader(int, int [3]);
  void readSolution(int, int, double&, double*);
  void readSolution(int, int, double&, int*);

};

//------------------------------------------------------------------------------

#endif
