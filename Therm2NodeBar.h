#ifndef _THERM2NODEBAR_H_
#define _THERM2NODEBAR_H_

#include <Element.h>

class Therm2NodeBar: public Element {


public:

	Therm2NodeBar(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 9; }

	void binWriteNodes(BinFileHandler &);
};
#endif
