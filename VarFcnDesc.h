#ifndef _VAR_FCN_DESC_H_
#define _VAR_FCN_DESC_H_

#include <VarFcn.h>

//------------------------------------------------------------------------------

class VarFcnEuler3D : public VarFcn {

  void computedUdV(double *, double *);
  void computedVdU(double *, double *);

public:

  VarFcnEuler3D(double);
  ~VarFcnEuler3D() {}

  void conservativeToPrimitive(double *, double *);
  void primitiveToConservative(double *, double *);
  void multiplyBydVdU(double *, double *, double *);
  void preMultiplyBydUdV(double *, double *, double *);
  void postMultiplyBydVdU(double *, double *, double *);
  void postMultiplyBydUdV(double *, double *, double *);

};

//------------------------------------------------------------------------------

inline
VarFcnEuler3D::VarFcnEuler3D(double g) : VarFcn(g) 
{

  pname = new const char*[5];

  pname[0] = "density";
  pname[1] = "x-velocity";
  pname[2] = "y-velocity";
  pname[3] = "z-velocity";
  pname[4] = "pressure";

}

//------------------------------------------------------------------------------

inline
void VarFcnEuler3D::conservativeToPrimitive(double *U, double *V)
{
  
  V[0] = U[0];

  double invRho = 1.0 / U[0];

  V[1] = U[1] * invRho;
  V[2] = U[2] * invRho;
  V[3] = U[3] * invRho;

  double vel2 = V[1] * V[1] + V[2] * V[2] + V[3] * V[3];

  V[4] = gam1 * (U[4] - 0.5 * U[0] * vel2);

}

//------------------------------------------------------------------------------

inline
void VarFcnEuler3D::primitiveToConservative(double *V, double *U)
{

  double vel2 = V[1] * V[1] + V[2] * V[2] + V[3] * V[3];

  U[0] = V[0];
  U[1] = V[0] * V[1];
  U[2] = V[0] * V[2];
  U[3] = V[0] * V[3];
  U[4] = V[4] * invgam1 + 0.5 * V[0] * vel2;

}

//------------------------------------------------------------------------------

inline
void VarFcnEuler3D::computedVdU(double *V, double *dVdU)
{

  double invrho = 1.0 / V[0];

  dVdU[0]  = 1.0;

  dVdU[5]  = -invrho * V[1];
  dVdU[6]  = invrho;

  dVdU[10] = -invrho * V[2];
  dVdU[12] = invrho;

  dVdU[15] = -invrho * V[3];
  dVdU[18] = invrho;

  dVdU[20] = gam1 * 0.5 * (V[1]*V[1] + V[2]*V[2] + V[3]*V[3]);
  dVdU[21] = -gam1 * V[1];
  dVdU[22] = -gam1 * V[2];
  dVdU[23] = -gam1 * V[3];
  dVdU[24] = gam1;

}

//------------------------------------------------------------------------------

inline
void VarFcnEuler3D::computedUdV(double *V, double *dUdV)
{

  dUdV[0]  = 1.0;

  dUdV[5]  = V[1];
  dUdV[6]  = V[0];

  dUdV[10] = V[2];
  dUdV[12] = V[0];

  dUdV[15] = V[3];
  dUdV[18] = V[0];

  dUdV[20] = 0.5 * (V[1]*V[1] + V[2]*V[2] + V[3]*V[3]);
  dUdV[21] = V[0] * V[1];
  dUdV[22] = V[0] * V[2];
  dUdV[23] = V[0] * V[3];
  dUdV[24] = invgam1;

}

//------------------------------------------------------------------------------

inline
void VarFcnEuler3D::multiplyBydVdU(double *V, double *vec, double *res)
{

  double dVdU[25];
  computedVdU(V, dVdU);

  res[0] = dVdU[0]*vec[0];
  res[1] = dVdU[5]*vec[0]+dVdU[6]*vec[1];
  res[2] = dVdU[10]*vec[0]+dVdU[12]*vec[2];
  res[3] = dVdU[15]*vec[0]+dVdU[18]*vec[3];
  res[4] = dVdU[20]*vec[0]+dVdU[21]*vec[1]+dVdU[22]*vec[2]+dVdU[23]*vec[3]+dVdU[24]*vec[4];

}

//------------------------------------------------------------------------------

inline
void VarFcnEuler3D::preMultiplyBydUdV(double *V, double *mat, double *res)
{

  double dUdV[25];
  computedUdV(V, dUdV);

  res[0] = dUdV[0]*mat[0];
  res[1] = dUdV[0]*mat[1];
  res[2] = dUdV[0]*mat[2];
  res[3] = dUdV[0]*mat[3];
  res[4] = dUdV[0]*mat[4];
  res[5] = dUdV[5]*mat[0]+dUdV[6]*mat[5];
  res[6] = dUdV[5]*mat[1]+dUdV[6]*mat[6];
  res[7] = dUdV[5]*mat[2]+dUdV[6]*mat[7];
  res[8] = dUdV[5]*mat[3]+dUdV[6]*mat[8];
  res[9] = dUdV[5]*mat[4]+dUdV[6]*mat[9];
  res[10] = dUdV[10]*mat[0]+dUdV[12]*mat[10];
  res[11] = dUdV[10]*mat[1]+dUdV[12]*mat[11];
  res[12] = dUdV[10]*mat[2]+dUdV[12]*mat[12];
  res[13] = dUdV[10]*mat[3]+dUdV[12]*mat[13];
  res[14] = dUdV[10]*mat[4]+dUdV[12]*mat[14];
  res[15] = dUdV[15]*mat[0]+dUdV[18]*mat[15];
  res[16] = dUdV[15]*mat[1]+dUdV[18]*mat[16];
  res[17] = dUdV[15]*mat[2]+dUdV[18]*mat[17];
  res[18] = dUdV[15]*mat[3]+dUdV[18]*mat[18];
  res[19] = dUdV[15]*mat[4]+dUdV[18]*mat[19];
  res[20] = dUdV[20]*mat[0]+dUdV[21]*mat[5]+dUdV[22]*mat[10]+dUdV[23]*mat[15]+dUdV[24]*mat[20];
  res[21] = dUdV[20]*mat[1]+dUdV[21]*mat[6]+dUdV[22]*mat[11]+dUdV[23]*mat[16]+dUdV[24]*mat[21];
  res[22] = dUdV[20]*mat[2]+dUdV[21]*mat[7]+dUdV[22]*mat[12]+dUdV[23]*mat[17]+dUdV[24]*mat[22];
  res[23] = dUdV[20]*mat[3]+dUdV[21]*mat[8]+dUdV[22]*mat[13]+dUdV[23]*mat[18]+dUdV[24]*mat[23];
  res[24] = dUdV[20]*mat[4]+dUdV[21]*mat[9]+dUdV[22]*mat[14]+dUdV[23]*mat[19]+dUdV[24]*mat[24];

}

//------------------------------------------------------------------------------

inline
void VarFcnEuler3D::postMultiplyBydVdU(double *V, double *mat, double *res)
{

  double dVdU[25];
  computedVdU(V, dVdU);

  res[0] = mat[0]*dVdU[0]+mat[1]*dVdU[5]+mat[2]*dVdU[10]+mat[3]*dVdU[15]+mat[4]*dVdU[20];
  res[1] = mat[1]*dVdU[6]+mat[4]*dVdU[21];
  res[2] = mat[2]*dVdU[12]+mat[4]*dVdU[22];
  res[3] = mat[3]*dVdU[18]+mat[4]*dVdU[23];
  res[4] = mat[4]*dVdU[24];
  res[5] = mat[5]*dVdU[0]+mat[6]*dVdU[5]+mat[7]*dVdU[10]+mat[8]*dVdU[15]+mat[9]*dVdU[20];
  res[6] = mat[6]*dVdU[6]+mat[9]*dVdU[21];
  res[7] = mat[7]*dVdU[12]+mat[9]*dVdU[22];
  res[8] = mat[8]*dVdU[18]+mat[9]*dVdU[23];
  res[9] = mat[9]*dVdU[24];
  res[10] = mat[10]*dVdU[0]+mat[11]*dVdU[5]+mat[12]*dVdU[10]+mat[13]*dVdU[15]+mat[14]*dVdU[20];
  res[11] = mat[11]*dVdU[6]+mat[14]*dVdU[21];
  res[12] = mat[12]*dVdU[12]+mat[14]*dVdU[22];
  res[13] = mat[13]*dVdU[18]+mat[14]*dVdU[23];
  res[14] = mat[14]*dVdU[24];
  res[15] = mat[15]*dVdU[0]+mat[16]*dVdU[5]+mat[17]*dVdU[10]+mat[18]*dVdU[15]+mat[19]*dVdU[20];
  res[16] = mat[16]*dVdU[6]+mat[19]*dVdU[21];
  res[17] = mat[17]*dVdU[12]+mat[19]*dVdU[22];
  res[18] = mat[18]*dVdU[18]+mat[19]*dVdU[23];
  res[19] = mat[19]*dVdU[24];
  res[20] = mat[20]*dVdU[0]+mat[21]*dVdU[5]+mat[22]*dVdU[10]+mat[23]*dVdU[15]+mat[24]*dVdU[20];
  res[21] = mat[21]*dVdU[6]+mat[24]*dVdU[21];
  res[22] = mat[22]*dVdU[12]+mat[24]*dVdU[22];
  res[23] = mat[23]*dVdU[18]+mat[24]*dVdU[23];
  res[24] = mat[24]*dVdU[24];

}

//------------------------------------------------------------------------------

inline
void VarFcnEuler3D::postMultiplyBydUdV(double *V, double *mat, double *res)
{

  double dUdV[25];
  computedUdV(V, dUdV);

  res[0] = mat[0]*dUdV[0]+mat[1]*dUdV[5]+mat[2]*dUdV[10]+mat[3]*dUdV[15]+mat[4]*dUdV[20];
  res[1] = mat[1]*dUdV[6]+mat[4]*dUdV[21];
  res[2] = mat[2]*dUdV[12]+mat[4]*dUdV[22];
  res[3] = mat[3]*dUdV[18]+mat[4]*dUdV[23];
  res[4] = mat[4]*dUdV[24];
  res[5] = mat[5]*dUdV[0]+mat[6]*dUdV[5]+mat[7]*dUdV[10]+mat[8]*dUdV[15]+mat[9]*dUdV[20];
  res[6] = mat[6]*dUdV[6]+mat[9]*dUdV[21];
  res[7] = mat[7]*dUdV[12]+mat[9]*dUdV[22];
  res[8] = mat[8]*dUdV[18]+mat[9]*dUdV[23];
  res[9] = mat[9]*dUdV[24];
  res[10] = mat[10]*dUdV[0]+mat[11]*dUdV[5]+mat[12]*dUdV[10]+mat[13]*dUdV[15]+mat[14]*dUdV[20];
  res[11] = mat[11]*dUdV[6]+mat[14]*dUdV[21];
  res[12] = mat[12]*dUdV[12]+mat[14]*dUdV[22];
  res[13] = mat[13]*dUdV[18]+mat[14]*dUdV[23];
  res[14] = mat[14]*dUdV[24];
  res[15] = mat[15]*dUdV[0]+mat[16]*dUdV[5]+mat[17]*dUdV[10]+mat[18]*dUdV[15]+mat[19]*dUdV[20];
  res[16] = mat[16]*dUdV[6]+mat[19]*dUdV[21];
  res[17] = mat[17]*dUdV[12]+mat[19]*dUdV[22];
  res[18] = mat[18]*dUdV[18]+mat[19]*dUdV[23];
  res[19] = mat[19]*dUdV[24];
  res[20] = mat[20]*dUdV[0]+mat[21]*dUdV[5]+mat[22]*dUdV[10]+mat[23]*dUdV[15]+mat[24]*dUdV[20];
  res[21] = mat[21]*dUdV[6]+mat[24]*dUdV[21];
  res[22] = mat[22]*dUdV[12]+mat[24]*dUdV[22];
  res[23] = mat[23]*dUdV[18]+mat[24]*dUdV[23];
  res[24] = mat[24]*dUdV[24];

}


//------------------------------------------------------------------------------

class VarFcnSA3D : public VarFcn {

  void computedUdV(double *, double *);
  void computedVdU(double *, double *);

public:

  VarFcnSA3D(double);
  ~VarFcnSA3D() {}

  void conservativeToPrimitive(double *, double *);
  void primitiveToConservative(double *, double *);
  void multiplyBydVdU(double *, double *, double *);
  void preMultiplyBydUdV(double *, double *, double *);
  void postMultiplyBydVdU(double *, double *, double *);
  void postMultiplyBydUdV(double *, double *, double *);

};

//------------------------------------------------------------------------------

inline
VarFcnSA3D::VarFcnSA3D(double g) : VarFcn(g) 
{

  pname = new const char*[6];

  pname[0] = "density";
  pname[1] = "x-velocity";
  pname[2] = "y-velocity";
  pname[3] = "z-velocity";
  pname[4] = "pressure";
  pname[5] = "nut";

}

//------------------------------------------------------------------------------

inline
void VarFcnSA3D::conservativeToPrimitive(double *U, double *V)
{
  
  V[0] = U[0];

  double invRho = 1.0 / U[0];

  V[1] = U[1] * invRho;
  V[2] = U[2] * invRho;
  V[3] = U[3] * invRho;

  double vel2 = V[1] * V[1] + V[2] * V[2] + V[3] * V[3];

  V[4] = gam1 * (U[4] - 0.5 * U[0] * vel2);

  V[5] = U[5] * invRho;

}

//------------------------------------------------------------------------------

inline
void VarFcnSA3D::primitiveToConservative(double *V, double *U)
{

  fprintf(stderr, "*** Error: primitiveToConservative not implemented\n");
 
}


//------------------------------------------------------------------------------

inline
void VarFcnSA3D::computedUdV(double *V, double *dUdV)
{

  dUdV[0]  = 1.0;

  dUdV[6]  = V[1];
  dUdV[7]  = V[0];

  dUdV[12] = V[2];
  dUdV[14] = V[0];

  dUdV[18] = V[3];
  dUdV[21] = V[0];

  dUdV[24] = 0.5 * (V[1]*V[1] + V[2]*V[2] + V[3]*V[3]);
  dUdV[25] = V[0] * V[1];
  dUdV[26] = V[0] * V[2];
  dUdV[27] = V[0] * V[3];
  dUdV[28] = invgam1;

  dUdV[30] = V[5];
  dUdV[35] = V[0];

}

//------------------------------------------------------------------------------

inline
void VarFcnSA3D::computedVdU(double *V, double *dVdU)
{

  double invrho = 1.0 / V[0];

  dVdU[0]  = 1.0;

  dVdU[6]  = -invrho * V[1];
  dVdU[7]  = invrho;

  dVdU[12] = -invrho * V[2];
  dVdU[14] = invrho;

  dVdU[18] = -invrho * V[3];
  dVdU[21] = invrho;

  dVdU[24] = gam1 * 0.5 * (V[1]*V[1] + V[2]*V[2] + V[3]*V[3]);
  dVdU[25] = -gam1 * V[1];
  dVdU[26] = -gam1 * V[2];
  dVdU[27] = -gam1 * V[3];
  dVdU[28] = gam1;

  dVdU[30] = -invrho * V[5];
  dVdU[35] = invrho;

}

//------------------------------------------------------------------------------

inline
void VarFcnSA3D::multiplyBydVdU(double *V, double *vec, double *res)
{

  fprintf(stderr, "*** Error: multiplyBydVdU not implemented\n");

}

//------------------------------------------------------------------------------

inline
void VarFcnSA3D::preMultiplyBydUdV(double *V, double *mat, double *res)
{

  double dUdV[36];
  computedUdV(V, dUdV);

  res[0] = dUdV[0]*mat[0];
  res[1] = dUdV[0]*mat[1];
  res[2] = dUdV[0]*mat[2];
  res[3] = dUdV[0]*mat[3];
  res[4] = dUdV[0]*mat[4];
  res[5] = dUdV[0]*mat[5];
  res[6] = dUdV[6]*mat[0]+dUdV[7]*mat[6];
  res[7] = dUdV[6]*mat[1]+dUdV[7]*mat[7];
  res[8] = dUdV[6]*mat[2]+dUdV[7]*mat[8];
  res[9] = dUdV[6]*mat[3]+dUdV[7]*mat[9];
  res[10] = dUdV[6]*mat[4]+dUdV[7]*mat[10];
  res[11] = dUdV[6]*mat[5]+dUdV[7]*mat[11];
  res[12] = dUdV[12]*mat[0]+dUdV[14]*mat[12];
  res[13] = dUdV[12]*mat[1]+dUdV[14]*mat[13];
  res[14] = dUdV[12]*mat[2]+dUdV[14]*mat[14];
  res[15] = dUdV[12]*mat[3]+dUdV[14]*mat[15];
  res[16] = dUdV[12]*mat[4]+dUdV[14]*mat[16];
  res[17] = dUdV[12]*mat[5]+dUdV[14]*mat[17];
  res[18] = dUdV[18]*mat[0]+dUdV[21]*mat[18];
  res[19] = dUdV[18]*mat[1]+dUdV[21]*mat[19];
  res[20] = dUdV[18]*mat[2]+dUdV[21]*mat[20];
  res[21] = dUdV[18]*mat[3]+dUdV[21]*mat[21];
  res[22] = dUdV[18]*mat[4]+dUdV[21]*mat[22];
  res[23] = dUdV[18]*mat[5]+dUdV[21]*mat[23];
  res[24] = dUdV[24]*mat[0]+dUdV[25]*mat[6]+dUdV[26]*mat[12]+dUdV[27]*mat[18]+dUdV[28]*mat[24];
  res[25] = dUdV[24]*mat[1]+dUdV[25]*mat[7]+dUdV[26]*mat[13]+dUdV[27]*mat[19]+dUdV[28]*mat[25];
  res[26] = dUdV[24]*mat[2]+dUdV[25]*mat[8]+dUdV[26]*mat[14]+dUdV[27]*mat[20]+dUdV[28]*mat[26];
  res[27] = dUdV[24]*mat[3]+dUdV[25]*mat[9]+dUdV[26]*mat[15]+dUdV[27]*mat[21]+dUdV[28]*mat[27];
  res[28] = dUdV[24]*mat[4]+dUdV[25]*mat[10]+dUdV[26]*mat[16]+dUdV[27]*mat[22]+dUdV[28]*mat[28];
  res[29] = dUdV[24]*mat[5]+dUdV[25]*mat[11]+dUdV[26]*mat[17]+dUdV[27]*mat[23]+dUdV[28]*mat[29];
  res[30] = dUdV[30]*mat[0]+dUdV[35]*mat[30];
  res[31] = dUdV[30]*mat[1]+dUdV[35]*mat[31];
  res[32] = dUdV[30]*mat[2]+dUdV[35]*mat[32];
  res[33] = dUdV[30]*mat[3]+dUdV[35]*mat[33];
  res[34] = dUdV[30]*mat[4]+dUdV[35]*mat[34];
  res[35] = dUdV[30]*mat[5]+dUdV[35]*mat[35];
  
}

//------------------------------------------------------------------------------

inline
void VarFcnSA3D::postMultiplyBydVdU(double *V, double *mat, double *res)
{

  double dVdU[36];
  computedVdU(V, dVdU);

  res[0] = mat[0]*dVdU[0]+mat[1]*dVdU[6]+mat[2]*dVdU[12]+
    mat[3]*dVdU[18]+mat[4]*dVdU[24]+mat[5]*dVdU[30];
  res[1] = mat[1]*dVdU[7]+mat[4]*dVdU[25];
  res[2] = mat[2]*dVdU[14]+mat[4]*dVdU[26];
  res[3] = mat[3]*dVdU[21]+mat[4]*dVdU[27];
  res[4] = mat[4]*dVdU[28];
  res[5] = mat[5]*dVdU[35];
  res[6] = mat[6]*dVdU[0]+mat[7]*dVdU[6]+mat[8]*dVdU[12]+
    mat[9]*dVdU[18]+mat[10]*dVdU[24]+mat[11]*dVdU[30];
  res[7] = mat[7]*dVdU[7]+mat[10]*dVdU[25];
  res[8] = mat[8]*dVdU[14]+mat[10]*dVdU[26];
  res[9] = mat[9]*dVdU[21]+mat[10]*dVdU[27];
  res[10] = mat[10]*dVdU[28];
  res[11] = mat[11]*dVdU[35];
  res[12] = mat[12]*dVdU[0]+mat[13]*dVdU[6]+mat[14]*dVdU[12]+
    mat[15]*dVdU[18]+mat[16]*dVdU[24]+mat[17]*dVdU[30];
  res[13] = mat[13]*dVdU[7]+mat[16]*dVdU[25];
  res[14] = mat[14]*dVdU[14]+mat[16]*dVdU[26];
  res[15] = mat[15]*dVdU[21]+mat[16]*dVdU[27];
  res[16] = mat[16]*dVdU[28];
  res[17] = mat[17]*dVdU[35];
  res[18] = mat[18]*dVdU[0]+mat[19]*dVdU[6]+mat[20]*dVdU[12]+
    mat[21]*dVdU[18]+mat[22]*dVdU[24]+mat[23]*dVdU[30];
  res[19] = mat[19]*dVdU[7]+mat[22]*dVdU[25];
  res[20] = mat[20]*dVdU[14]+mat[22]*dVdU[26];
  res[21] = mat[21]*dVdU[21]+mat[22]*dVdU[27];
  res[22] = mat[22]*dVdU[28];
  res[23] = mat[23]*dVdU[35];
  res[24] = mat[24]*dVdU[0]+mat[25]*dVdU[6]+mat[26]*dVdU[12]+
    mat[27]*dVdU[18]+mat[28]*dVdU[24]+mat[29]*dVdU[30];
  res[25] = mat[25]*dVdU[7]+mat[28]*dVdU[25];
  res[26] = mat[26]*dVdU[14]+mat[28]*dVdU[26];
  res[27] = mat[27]*dVdU[21]+mat[28]*dVdU[27];
  res[28] = mat[28]*dVdU[28];
  res[29] = mat[29]*dVdU[35];
  res[30] = mat[30]*dVdU[0]+mat[31]*dVdU[6]+mat[32]*dVdU[12]+
    mat[33]*dVdU[18]+mat[34]*dVdU[24]+mat[35]*dVdU[30];
  res[31] = mat[31]*dVdU[7]+mat[34]*dVdU[25];
  res[32] = mat[32]*dVdU[14]+mat[34]*dVdU[26];
  res[33] = mat[33]*dVdU[21]+mat[34]*dVdU[27];
  res[34] = mat[34]*dVdU[28];
  res[35] = mat[35]*dVdU[35];

}

//------------------------------------------------------------------------------

inline
void VarFcnSA3D::postMultiplyBydUdV(double *V, double *mat, double *res)
{

  fprintf(stderr, "*** Error: postMultiplyBydUdV not implemented\n");

}

//------------------------------------------------------------------------------

class VarFcnKE3D : public VarFcn {

  void computedUdV(double *, double *);
  void computedVdU(double *, double *);

public:

  VarFcnKE3D(double);
  ~VarFcnKE3D() {}

  void conservativeToPrimitive(double *, double *);
  void primitiveToConservative(double *, double *);
  void multiplyBydVdU(double *, double *, double *);
  void preMultiplyBydUdV(double *, double *, double *);
  void postMultiplyBydVdU(double *, double *, double *);
  void postMultiplyBydUdV(double *, double *, double *);

  double getTurbulentKineticEnergy(double *V) { return V[5]; }
  double getTurbulentDissipationRate(double *V) { return V[6]; }

};

//------------------------------------------------------------------------------

inline
VarFcnKE3D::VarFcnKE3D(double g) : VarFcn(g) 
{

  pname = new const char*[7];

  pname[0] = "density";
  pname[1] = "x-velocity";
  pname[2] = "y-velocity";
  pname[3] = "z-velocity";
  pname[4] = "pressure";
  pname[5] = "k";
  pname[6] = "epsilon";

}

//------------------------------------------------------------------------------

inline
void VarFcnKE3D::conservativeToPrimitive(double *U, double *V)
{
  
  V[0] = U[0];

  double invRho = 1.0 / U[0];

  V[1] = U[1] * invRho;
  V[2] = U[2] * invRho;
  V[3] = U[3] * invRho;

  double vel2 = V[1] * V[1] + V[2] * V[2] + V[3] * V[3];

  V[4] = gam1 * (U[4] - 0.5 * U[0] * vel2);

  V[5] = U[5] * invRho;
  V[6] = U[6] * invRho;

}

//------------------------------------------------------------------------------

inline
void VarFcnKE3D::primitiveToConservative(double *V, double *U)
{

  fprintf(stderr, "*** Error: primitiveToConservative not implemented\n");
 
}

//------------------------------------------------------------------------------

inline
void VarFcnKE3D::computedUdV(double *V, double *dUdV)
{

  dUdV[0]  = 1.0;

  dUdV[7]  = V[1];
  dUdV[8]  = V[0];

  dUdV[14] = V[2];
  dUdV[16] = V[0];

  dUdV[21] = V[3];
  dUdV[24] = V[0];

  dUdV[28] = 0.5 * (V[1]*V[1] + V[2]*V[2] + V[3]*V[3]);
  dUdV[29] = V[0] * V[1];
  dUdV[30] = V[0] * V[2];
  dUdV[31] = V[0] * V[3];
  dUdV[32] = invgam1;

  dUdV[35] = V[5];
  dUdV[40] = V[0];

  dUdV[42] = V[6];
  dUdV[48] = V[0];

}

//------------------------------------------------------------------------------

inline
void VarFcnKE3D::computedVdU(double *V, double *dVdU)
{

  double invrho = 1.0 / V[0];

  dVdU[0]  = 1.0;

  dVdU[7]  = -invrho * V[1];
  dVdU[8]  = invrho;

  dVdU[14] = -invrho * V[2];
  dVdU[16] = invrho;

  dVdU[21] = -invrho * V[3];
  dVdU[24] = invrho;

  dVdU[28] = gam1 * 0.5 * (V[1]*V[1] + V[2]*V[2] + V[3]*V[3]);
  dVdU[29] = -gam1 * V[1];
  dVdU[30] = -gam1 * V[2];
  dVdU[31] = -gam1 * V[3];
  dVdU[32] = gam1;

  dVdU[35] = -invrho * V[5];
  dVdU[40] = invrho;

  dVdU[42] = -invrho * V[6];
  dVdU[48] = invrho;

}

//------------------------------------------------------------------------------

inline
void VarFcnKE3D::multiplyBydVdU(double *V, double *vec, double *res)
{

  fprintf(stderr, "*** Error: multiplyBydVdU not implemented\n");

}

//------------------------------------------------------------------------------

inline
void VarFcnKE3D::preMultiplyBydUdV(double *V, double *mat, double *res)
{

  double dUdV[49];
  computedUdV(V, dUdV);

  res[0] = dUdV[0]*mat[0];
  res[1] = dUdV[0]*mat[1];
  res[2] = dUdV[0]*mat[2];
  res[3] = dUdV[0]*mat[3];
  res[4] = dUdV[0]*mat[4];
  res[5] = dUdV[0]*mat[5];
  res[6] = dUdV[0]*mat[6];
  res[7] = dUdV[7]*mat[0]+dUdV[8]*mat[7];
  res[8] = dUdV[7]*mat[1]+dUdV[8]*mat[8];
  res[9] = dUdV[7]*mat[2]+dUdV[8]*mat[9];
  res[10] = dUdV[7]*mat[3]+dUdV[8]*mat[10];
  res[11] = dUdV[7]*mat[4]+dUdV[8]*mat[11];
  res[12] = dUdV[7]*mat[5]+dUdV[8]*mat[12];
  res[13] = dUdV[7]*mat[6]+dUdV[8]*mat[13];
  res[14] = dUdV[14]*mat[0]+dUdV[16]*mat[14];
  res[15] = dUdV[14]*mat[1]+dUdV[16]*mat[15];
  res[16] = dUdV[14]*mat[2]+dUdV[16]*mat[16];
  res[17] = dUdV[14]*mat[3]+dUdV[16]*mat[17];
  res[18] = dUdV[14]*mat[4]+dUdV[16]*mat[18];
  res[19] = dUdV[14]*mat[5]+dUdV[16]*mat[19];
  res[20] = dUdV[14]*mat[6]+dUdV[16]*mat[20];
  res[21] = dUdV[21]*mat[0]+dUdV[24]*mat[21];
  res[22] = dUdV[21]*mat[1]+dUdV[24]*mat[22];
  res[23] = dUdV[21]*mat[2]+dUdV[24]*mat[23];
  res[24] = dUdV[21]*mat[3]+dUdV[24]*mat[24];
  res[25] = dUdV[21]*mat[4]+dUdV[24]*mat[25];
  res[26] = dUdV[21]*mat[5]+dUdV[24]*mat[26];
  res[27] = dUdV[21]*mat[6]+dUdV[24]*mat[27];
  res[28] = dUdV[28]*mat[0]+dUdV[29]*mat[7]+dUdV[30]*mat[14]+dUdV[31]*mat[21]+dUdV[32]*mat[28];
  res[29] = dUdV[28]*mat[1]+dUdV[29]*mat[8]+dUdV[30]*mat[15]+dUdV[31]*mat[22]+dUdV[32]*mat[29];
  res[30] = dUdV[28]*mat[2]+dUdV[29]*mat[9]+dUdV[30]*mat[16]+dUdV[31]*mat[23]+dUdV[32]*mat[30];
  res[31] = dUdV[28]*mat[3]+dUdV[29]*mat[10]+dUdV[30]*mat[17]+dUdV[31]*mat[24]+dUdV[32]*mat[31];
  res[32] = dUdV[28]*mat[4]+dUdV[29]*mat[11]+dUdV[30]*mat[18]+dUdV[31]*mat[25]+dUdV[32]*mat[32];
  res[33] = dUdV[28]*mat[5]+dUdV[29]*mat[12]+dUdV[30]*mat[19]+dUdV[31]*mat[26]+dUdV[32]*mat[33];
  res[34] = dUdV[28]*mat[6]+dUdV[29]*mat[13]+dUdV[30]*mat[20]+dUdV[31]*mat[27]+dUdV[32]*mat[34];
  res[35] = dUdV[35]*mat[0]+dUdV[40]*mat[35];
  res[36] = dUdV[35]*mat[1]+dUdV[40]*mat[36];
  res[37] = dUdV[35]*mat[2]+dUdV[40]*mat[37];
  res[38] = dUdV[35]*mat[3]+dUdV[40]*mat[38];
  res[39] = dUdV[35]*mat[4]+dUdV[40]*mat[39];
  res[40] = dUdV[35]*mat[5]+dUdV[40]*mat[40];
  res[41] = dUdV[35]*mat[6]+dUdV[40]*mat[41];
  res[42] = dUdV[42]*mat[0]+dUdV[48]*mat[42];
  res[43] = dUdV[42]*mat[1]+dUdV[48]*mat[43];
  res[44] = dUdV[42]*mat[2]+dUdV[48]*mat[44];
  res[45] = dUdV[42]*mat[3]+dUdV[48]*mat[45];
  res[46] = dUdV[42]*mat[4]+dUdV[48]*mat[46];
  res[47] = dUdV[42]*mat[5]+dUdV[48]*mat[47];
  res[48] = dUdV[42]*mat[6]+dUdV[48]*mat[48];

}

//------------------------------------------------------------------------------

inline
void VarFcnKE3D::postMultiplyBydVdU(double *V, double *mat, double *res)
{

  double dVdU[49];
  computedVdU(V, dVdU);

  res[0] = mat[0]*dVdU[0]+mat[1]*dVdU[7]+mat[2]*dVdU[14]+mat[3]*dVdU[21]+
    mat[4]*dVdU[28]+mat[5]*dVdU[35]+mat[6]*dVdU[42];
  res[1] = mat[1]*dVdU[8]+mat[4]*dVdU[29];
  res[2] = mat[2]*dVdU[16]+mat[4]*dVdU[30];
  res[3] = mat[3]*dVdU[24]+mat[4]*dVdU[31];
  res[4] = mat[4]*dVdU[32];
  res[5] = mat[5]*dVdU[40];
  res[6] = mat[6]*dVdU[48];
  res[7] = mat[7]*dVdU[0]+mat[8]*dVdU[7]+mat[9]*dVdU[14]+mat[10]*dVdU[21]+
    mat[11]*dVdU[28]+mat[12]*dVdU[35]+mat[13]*dVdU[42];
  res[8] = mat[8]*dVdU[8]+mat[11]*dVdU[29];
  res[9] = mat[9]*dVdU[16]+mat[11]*dVdU[30];
  res[10] = mat[10]*dVdU[24]+mat[11]*dVdU[31];
  res[11] = mat[11]*dVdU[32];
  res[12] = mat[12]*dVdU[40];
  res[13] = mat[13]*dVdU[48];
  res[14] = mat[14]*dVdU[0]+mat[15]*dVdU[7]+mat[16]*dVdU[14]+mat[17]*dVdU[21]+
    mat[18]*dVdU[28]+mat[19]*dVdU[35]+mat[20]*dVdU[42];
  res[15] = mat[15]*dVdU[8]+mat[18]*dVdU[29];
  res[16] = mat[16]*dVdU[16]+mat[18]*dVdU[30];
  res[17] = mat[17]*dVdU[24]+mat[18]*dVdU[31];
  res[18] = mat[18]*dVdU[32];
  res[19] = mat[19]*dVdU[40];
  res[20] = mat[20]*dVdU[48];
  res[21] = mat[21]*dVdU[0]+mat[22]*dVdU[7]+mat[23]*dVdU[14]+mat[24]*dVdU[21]+
    mat[25]*dVdU[28]+mat[26]*dVdU[35]+mat[27]*dVdU[42];
  res[22] = mat[22]*dVdU[8]+mat[25]*dVdU[29];
  res[23] = mat[23]*dVdU[16]+mat[25]*dVdU[30];
  res[24] = mat[24]*dVdU[24]+mat[25]*dVdU[31];
  res[25] = mat[25]*dVdU[32];
  res[26] = mat[26]*dVdU[40];
  res[27] = mat[27]*dVdU[48];
  res[28] = mat[28]*dVdU[0]+mat[29]*dVdU[7]+mat[30]*dVdU[14]+mat[31]*dVdU[21]+
    mat[32]*dVdU[28]+mat[33]*dVdU[35]+mat[34]*dVdU[42];
  res[29] = mat[29]*dVdU[8]+mat[32]*dVdU[29];
  res[30] = mat[30]*dVdU[16]+mat[32]*dVdU[30];
  res[31] = mat[31]*dVdU[24]+mat[32]*dVdU[31];
  res[32] = mat[32]*dVdU[32];
  res[33] = mat[33]*dVdU[40];
  res[34] = mat[34]*dVdU[48];
  res[35] = mat[35]*dVdU[0]+mat[36]*dVdU[7]+mat[37]*dVdU[14]+mat[38]*dVdU[21]+
    mat[39]*dVdU[28]+mat[40]*dVdU[35]+mat[41]*dVdU[42];
  res[36] = mat[36]*dVdU[8]+mat[39]*dVdU[29];
  res[37] = mat[37]*dVdU[16]+mat[39]*dVdU[30];
  res[38] = mat[38]*dVdU[24]+mat[39]*dVdU[31];
  res[39] = mat[39]*dVdU[32];
  res[40] = mat[40]*dVdU[40];
  res[41] = mat[41]*dVdU[48];
  res[42] = mat[42]*dVdU[0]+mat[43]*dVdU[7]+mat[44]*dVdU[14]+mat[45]*dVdU[21]+
    mat[46]*dVdU[28]+mat[47]*dVdU[35]+mat[48]*dVdU[42];
  res[43] = mat[43]*dVdU[8]+mat[46]*dVdU[29];
  res[44] = mat[44]*dVdU[16]+mat[46]*dVdU[30];
  res[45] = mat[45]*dVdU[24]+mat[46]*dVdU[31];
  res[46] = mat[46]*dVdU[32];
  res[47] = mat[47]*dVdU[40];
  res[48] = mat[48]*dVdU[48];

}

//------------------------------------------------------------------------------

inline
void VarFcnKE3D::postMultiplyBydUdV(double *V, double *mat, double *res)
{

  fprintf(stderr, "*** Error: postMultiplyBydUdV not implemented\n");

}

//------------------------------------------------------------------------------

#endif
