#ifndef _FOURNODEQUAD_H_
#define _FOURNODEQUAD_H_

#include <Element.h>

class FourNodeQuad: public Element {

public:
	FourNodeQuad(int*);

	Element *clone();

	void renum(int *);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 2; }

        void binWriteNodes(BinFileHandler &);
};
#endif

