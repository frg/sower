#include 	<TimoshenkoBeam.h>

//---------------------------------------------------------

TimoshenkoBeam::TimoshenkoBeam(int* nodenums)
{
	nn = new int[3];

        nn[0] = nodenums[0];
        nn[1] = nodenums[1];
        nn[2] = nodenums[2];
}

//---------------------------------------------------------

int
TimoshenkoBeam::numNodes()
{
	return 2;
}

//---------------------------------------------------------

int *
TimoshenkoBeam::nodes(int *p)
{
	if(p == 0) p = new int[2];
	p[0] = nn[0];
	p[1] = nn[1];
	return p;
}

//---------------------------------------------------------

void TimoshenkoBeam::binWriteNodes(BinFileHandler &outfile)  {

  int type = 7;
  outfile.write(&type, 1);
  outfile.write(nn,3);
}
