//AUTHOR: Chris Saam
//FILE: Point3D.h
//CLASS PROVIDED : Point3D (A class of three dimensional
//points which are ttreated as linear algebra vectors
//along with basic linear algebra operators.
//CONSTRUCTOR for the Point3D class:
//	Point3D()
//	Postconditioni: the Point3D object has been initialized
//		to contain the origin (0,0,0);
//	Point3D(const double xIN,const double yIN,const double zIN)
//	Postcondition: the Point3D object has been initializex
//		to contain (xIN,yIN,zIN)
//MODIFICATION MEMBER OPERATOR for the Point3D class:
//	double operator[](const int position )
//	Precondition: 0<=postion<=2
//	Postcondition: the coordinate stored at point[position]
//		has been accessed and may be modified.
//CONSTANT MEMBER FUNCTIONS AND OPERATORS for Point3D:
//  Point3D operator *(const double s)const
//	Postcondition: the coordinates of the Point3D object
//		are multiplied by a scalar s of type double and the
//		result is returned.
//	Point3D operator /(const double s)const
//	Postcondition: the coordinates of the Point3D object
//		are deviided by a scalar s of type double and the 
//		result is returned.
//  Point3D operator +(const Point3D& p)const
//	Postcondition: the vector sum of two points is computed
//		and the result is returned.
//	Point3D operator -(const Point3D& p)const
//	Postcondition: the vector difference of two points is computed
//    and the result is returned.
//CONSTANT FRIEND FUNCTIONS AND OPERATORS for the Point3D class:
//	friend Point3D operator *(const double s, const Point3D& p1)
//	Postcondition: the coordinates of p1 are multiplied 
//		by a scalar s and returned.  This operator is similar to
//		to the member multiplication operator but adds communitivity.
//	friend double dotp(const Point3D& v1,const Point3D& v2)
//	Postcondition: the vector dot or scalar product of two
//		Point3D objects is computed and returned as a double type.
//	friend Point3D crossp(const Point3D& v1,const Point3D& v2)	
//	Postcondition: the vector cross product of two Point3D 
//		objects is computed and returned as a Point3D type.
//	friend double norm(const Point3D& v)
//	Postcondition: the vector two or Euclidean norm of a 
//	Point3D object is returned as a double type.
//INSERTION AND EXTRACTION OPERATOR for the Point3D class:
//	friend ostream& operator<<(ostream& os,const Point3D& source)
//	Postcondition: the coordinates of source are passed to the
//		output stream with a single space between each coordinate.
//		All coordinated will be printed on one line with no newline 
//		character.
//	friend istream& operator>>(istream& is,Point3D& source)
//	Postcondition: the coordinates of source are read
//		from the input stream is.
//VALUE SEMATICS for the Point3D class:
//	The assignment operator and copy constructor may be used with
//	the Point3D class and are both defaults.
#ifndef _POINT3D_H_
#define _POINT3D_H_
#include<cstdlib>
#include<cassert>
#include<iomanip>
#include<iostream>
using std::ostream;
using std::istream;
#include<cmath>


class Point3D{
protected:
	double point[3];
public:
  Point3D();
  Point3D(const double xIN,const double yIN,const double zIN);
	double& operator[](const int position );
  Point3D operator *(const double s)const
		{return (Point3D(s*point[0],s*point[1],s*point[2]));}
	Point3D operator /(const double s)const
		{return (Point3D(point[0]/s,point[1]/s,point[2]/s));}
	Point3D operator +(const Point3D& p)const
		{return (Point3D(point[0]+p.point[0],point[1]+p.point[1],
			point[2]+p.point[2]));}
	Point3D operator -(const Point3D& p)const
		{return (Point3D(point[0]-p.point[0],point[1]-p.point[1],
				point[2]-p.point[2]));}
	friend Point3D operator *(const double s, const Point3D& p1)
		{return(Point3D(s*p1.point[0],s*p1.point[1],s*p1.point[2]));}
	friend double dotp(const Point3D& v1,const Point3D& v2)
		{return(v1.point[0]*v2.point[0]+v1.point[1]*v2.point[1]
			+v1.point[2]*v2.point[2]);}
	friend Point3D crossp(const Point3D& v1,const Point3D& v2)
		{return(Point3D(v1.point[1]*v2.point[2]-v1.point[2]*v2.point[1],
			v1.point[2]*v2.point[0]-v1.point[0]*v2.point[2],
			v1.point[0]*v2.point[1]-v1.point[1]*v2.point[0]));}
	friend double norm(const Point3D& v)
		{return (sqrt(v.point[0]*v.point[0]+v.point[1]
			*v.point[1]+v.point[2]*v.point[2]));}
  friend ostream& operator<<(ostream& os,const Point3D& source)
	{return os<<source.point[0]<<' '<<source.point[1]<<' '<<source.point[2];}
  friend istream& operator>>(istream& is,Point3D& source)
	{return is>>source.point[0]>>source.point[1]>>source.point[2];}
  double *getPoint() {return point;}
};
#endif
