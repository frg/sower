#ifndef _EIGHTNODEBRICK_H_
#define _EIGHTNODEBRICK_H_

#include <Element.h>

class EightNodeBrick: public Element {

        double *C; 
public:
	EightNodeBrick(int*);

        int  numNodes();
        int* nodes(int * = 0);
        int getType()  { return 17; }

        void binWriteNodes(BinFileHandler &);

};
#endif

