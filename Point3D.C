#include "Point3D.h"

Point3D::Point3D(){
  point[0]=0.0;
  point[1]=0.0;
  point[2]=0.0;
}

Point3D::Point3D(const double xIN, const double yIN,
		const double zIN){
  point[0]=xIN;
  point[1]=yIN;
  point[2]=zIN;
}

double&
Point3D::operator[](const int position){
	assert(0<=position);
	assert(position<=2);
	return(point[position]);
}
