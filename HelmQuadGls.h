#ifndef _HELMQUADGLS_H_
#define _HELMQUADGLS_H_

#include <Element.h>

class HelmQuadGls: public Element {

public:
	HelmQuadGls(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 31; }

   	void binWriteNodes(BinFileHandler &);

};
#endif

