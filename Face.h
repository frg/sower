#ifndef _FACE_H_
#define _FACE_H_
#include<cstdlib>
#include<cassert>

#include <Element.h>

class Face : public Element  {
private:
//  int *nn;
  int bc;
  int surface_id;
public:
  Face() { nn = new int[3]; prop = 0; }
  ~Face() {};

  int *getFace() {return nn;}
  int &operator[](int i) { return nn[i]; }

  int 	numNodes() { return 3; }

  int*  nodes(int *p = 0) { 
       	if(p == 0) p = new int[3];
        p[0] = nn[0];
        p[1] = nn[1];
        p[2] = nn[2];

        return p;
        }

  void setBC(int code)  { bc = code; }
  int  getBC() { return bc; }
  int getType() { return 4; }
  void setSurfaceID(int id) { surface_id = id; }
  int getSurfaceID() { return surface_id; }
  
  void binWriteNodes(BinFileHandler &outfile) { outfile.write(nn, 3); }
};  

#endif
