#ifndef _FOURNODESHELL_H_
#define _FOURNODESHELL_H_

#include	<Element.h>


class FourNodeShell : public Element {


public:
	FourNodeShell(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 88; }

	void	binWriteNodes(BinFileHandler &);
};
#endif

