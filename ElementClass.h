//AUTHOR:Chris Saam
//FILE: ElementClass.h
//CLASS PROVIDED: ElementClass (a container class for the
//classes needed to contain structure elements.  Classes abstracted
//from ElementClass are expected to contain the array address of
//Point3D in an array the user provides.)
//CONSTRUCTORS for the ElementClass class:
//	ElementClass(){}
//	Postcondition: the ElementClass container object is
//		is initialized.
//MODIFICTION MEMBER FUNCTION for the Triangle3D class:
//	static ElementClass* build(int type,int size, int* nodes, int dim)
//	Postcondition: an ElementClass pointer is returned.  This
//		pointer points to an object of a type abstracted from
//		ElementClass which describes a structural element.  The fourth
//		argument dim relates the spatial dimension of the element.
//CONSTANT VIRTUAL FUNCTIONS for the ElementClass class:
//	virtual int numNodes() = 0;
//	Postcondition: the number of Point3D array addresses contained  
//		in an abstracted ElementClass is returned.
//	virtual int getNode(int position) = 0;
//	Postcondition: address stored at position
//		in the abstracted ElementClass is returned.
//	virtual void match(ResizeArray<Point3D*>& allPoints,
//	const Point3D& matchPoint, const double tol, MatchInfo& info)const =0
//	Postcondition: an attempt to map matchPoint to an abstracted 
//    ElementClass type is made and a struct of type MatchInfo is
//		returned.  The struct tells the user if the mapping was successfull.
//		If the mapping is successfull then the natural coordinated of the
//		the matched point is ialso returned along with the distance it lies
//		from matchPoint.
//INSERTION OPERATOR for ElementClass class:
//	friend ostream& operator<<(ostream& os,ElementClass& source)
//	Postcondition: the array addresses stored in source are sent
//		to the output stream os with single spaces and with out a 
//		newline character.  This operator should work for all 
//		abstract types.
#ifndef _ELEMENT_CLASS
#define _ELEMENT_CLASS

#include"Point3D.h"
#include"ResizeArray.h"
#include"tools.h"
#include"ElementTypes.h"
#include<cassert>
#include <iostream>
using std::cerr;
using std::endl;

extern int dimension;

struct MatchInfo {
		int success; // 1 indicate match, 0 non match
		double xi,eta; // Natural coordinates of the match
		Point3D point;
		double dist; // Distance between point and its match
};

class ElementClass{
public:
	ElementClass(){};
	static ElementClass* build(int type,int size, int* nodes);
  virtual int numNodes() = 0;
	virtual int getNode(int position) = 0;
	friend ostream& operator<<(ostream& os,ElementClass& source);
	virtual void match(ResizeArray<Point3D*>& allPoints,
	const Point3D& matchPoint, const double tol, MatchInfo& info)const =0  ;
};
#endif

