#include	<HelmTri3Gal.h>

HelmTri3Gal::HelmTri3Gal(int* nodenums)
{
	nn = new int[3];

	nn[0] = nodenums[0];
	nn[1] = nodenums[1];
	nn[2] = nodenums[2];
}


int
HelmTri3Gal::numNodes()
{
 	return 3;
}

int*
HelmTri3Gal::nodes(int *p)
{
 	if(p == 0) p = new int[3];
 	p[0] = nn[0];
 	p[1] = nn[1];
 	p[2] = nn[2];
	return p;
}

void HelmTri3Gal::binWriteNodes(BinFileHandler &outfile) {

  int type = 35;
  outfile.write(&type, 1);
  outfile.write(nn, 3);
}
