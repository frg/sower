#ifndef _HELMTRI3GLS_H_
#define _HELMTRI3GLS_H_

#include <Element.h>

class HelmTri3Gls: public Element {

public:
	HelmTri3Gls(int*);


        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 36; }

     	void binWriteNodes(BinFileHandler &);

};
#endif

