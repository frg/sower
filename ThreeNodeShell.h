#ifndef _THREENODESHELL_H_
#define _THREENODESHELL_H_

#include	<Element.h>


class ThreeNodeShell : public Element {


public:
	ThreeNodeShell(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 8; }

	void	binWriteNodes(BinFileHandler &);
};
#endif

