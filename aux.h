#ifndef _AUX_H_
#define _AUX_H_

#include<cstdio>
#include<cstdlib>
#include<fstream>
#include<cstring>
#include<cmath>
#include <iostream>
using std::cerr;
using std::endl;
#include "ResizeArray.h"
#include"Point3D.h"
#include"tools.h"
#include "Element.h"
struct OutputInfo {
   enum { Displacement, Temperature, StressXX,    StressYY,    StressZZ,
          StressXY,     StressYZ,    StressXZ,    StrainXX,    StrainYY,
          StrainZZ,     StrainXY,    StrainYZ,    StrainXZ,    HeatFlXX,
          HeatFlXY,     HeatFlXZ,    GrdTempX,    GrdTempY,    GrdTempZ,
          StressVM,     InXForce,    InYForce,    InZForce,    AXMoment,
          AYMoment,     AZMoment,    Energies,    AeroForce,   EigenPair,
          StrainVM,     Helmholtz,   Velocity,    Farfield };
   int   interval[32];
   char* filename[32];
   FILE* filptr[32];
   int   averageFlg[32];
};

//--------------------------------------------------------------------

typedef struct {
   int num;
   double xyz[3];
} NumedNode;

//--------------------------------------------------------------------

typedef struct {
   int num;
   int nd[500];
} NumList;

//--------------------------------------------------------------------

struct FrameData {
  int num;
  double d[9];
};

//--------------------------------------------------------------------

class BCList {
  public:
    BCond *d;
    int n;
    int maxbc;

    BCList();
    void add(BCond &);
    void add(int nd, int dof, double val) { BCond bc;
                     bc.nnum = nd; bc.dofnum =dof; bc.val = val; add(bc); }
};

//----------------------------------------------------------

struct interCoords{
	int success;
	int elNum;
	double xi;
	double eta;
	Point3D point;
	int domNum;
};

//----------------------------------------------------------

class CoefData {
      double c[6][6];
    public:
      void zero();
      void setCoef(int,int,double);
      double *values () { return (double *)c; }
};

//----------------------------------------------------------

struct LayerData {
  int lnum;
  double d[9];
};

//----------------------------------------------------------

// composite layer information
class LayInfo {
   public:
     int numLayers;
     int maxLayer;
     int type;
   public:
     double (*data)[9];
     double (*grad)[9];

     LayInfo(int _type);

     void add(int,double *);
     //void setGrad();
     //void zeroGrad();

     int nLayers() { return numLayers; }
     int getType() { return type; }

     double *values()  { return (double *) data; }
     double *gradval() { return (double *) grad; }

};

int yylex(void);
    
void yyerror(const char*);

#endif
