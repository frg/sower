#ifndef _ROTNSPRLINK_H_
#define _ROTNSPRLINK_H_

#include <Element.h>

class RotnSprlink : public Element {

public:

	RotnSprlink(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 22; }

   	void binWriteNodes(BinFileHandler &);
};
#endif
