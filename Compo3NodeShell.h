#ifndef _COMPO3NODESHELL_H_
#define _COMPO3NODESHELL_H_

#include <Element.h>

class BinFileHandler;

class Compo3NodeShell : public Element {

        int      type;
        int numLayers;
        int     (*idlay)[5];
        double  *layData;
        double  *cCoefs;
        double  *cFrame;

public:
	Compo3NodeShell(int*);

        int  numNodes();
        int* nodes(int * = 0);
        int getType() { return 20; }
  
        void binWriteNodes(BinFileHandler &);

};
#endif

