#ifndef _TENNODETETRAHEDRAL_H_
#define _TENNODETETRAHEDRAL_H_

#include <Element.h>

class TenNodeTetrahedral: public Element {

public:
	TenNodeTetrahedral(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 25; }

 	void binWriteNodes(BinFileHandler &);
};
#endif

