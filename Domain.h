#ifndef _DOMAIN_
#define _DOMAIN_

#include"Point3D.h"
#include"Face.h"
#include"Connectivity.h"
#include"aux.h"
#include"Decomp3D.h"
#include"Element.h"
#include"BlockAlloc.h"
#include"ResizeArray.h"

//----------------------------------------------------------------

struct MatchData  {
  int numPts;
  int *pts;
  double (*data)[2];
};

//----------------------------------------------------------------

// Element attribute structure
struct Attrib {
  int nele;
  int attr;
  int cmp_attr, cmp_frm;
  double cmp_theta;
};

//----------------------------------------------------------------

// Element Frame Data structure
struct EFrameData {
    int elnum;
    EFrame frame;
    EFrameData *next;
};

//----------------------------------------------------------------

class Domain {

protected: 

  int numClusters;
  int numNode;
  int numElem;

  ResizeArray<Point3D > node;
  ResizeArray<Element *> elements;

  Decomp3D *decomp;

  int **clusterNodeMap;
  MapVec *gl2ClNodeMap;
  MapVec *gl2ClElemMap;
  MapVec *gl2ClMatchMap;

  // PJSA: Boundary Conditions Data
  int numDirichlet;          // number of dirichlet bc
  BCond* dbc;                // set of those dirichlet bc
  int *numDbcPerClus;
  int **dbcPerClus;
                                                                                                             
  int numNeuman;             // number of Neuman bc
  BCond* nbc;                // set of Neuman bc
  int *numNbcPerClus;
  int **nbcPerClus;
                                                                                                             
  int numConvBC;             // number of convective bc
  BCond* cvbc;               // set of convective bc
  int *numCvbcPerClus;
  int **cvbcPerClus;
                                                                                                             
  int numIDis;               // number of Initial displacements
  BCond *iDis;               // set of those initial displacements
  int *numIDisPerClus;
  int **iDisPerClus;
                                                                                                             
  int numIDis6;              // num of Initial displacements (6 column)
  BCond* iDis6;              // set of those intitial displacements
  int *numIDis6PerClus;
  int **iDis6PerClus;
                                                                                                             
  int numIVel;               // number of initial velocities
  BCond *iVel;               // set of those initial velocities
  int *numIvelPerClus;
  int **iVelPerClus;
                                                                                                             
  int numITemp;              // number of initial temperatures
  BCond *iTemp;              // set of those intitial temperatures
  int *numItempPerClus;
  int **iTempPerClus;


public:

  Domain(int);
  ~Domain() {};

  virtual void getGeometry(const char *) = 0;
  void renumberBC(MapVec &, int);
  void initBCData();
  virtual void getMatchInfo(const char *name) = 0;
  virtual Connectivity *getBinaryGeometry(const char *) = 0;
  virtual void getBinaryMatchInfo(const char *name) = 0;
  void getDecomp(const char *name, int &, int *);

  virtual void makeConnectivity(Connectivity *);
  void createClusterToGlobalNodeMap();
  virtual void renumberNodes();

  void printCpuToSubMap(int, const char *, int, int);
  virtual void printGlobalConnectivity(const char *); 
  void printRangesInfo(BinFileHandler &, int, Connectivity *, MapVec &);

  virtual void printGeometry(const char *) = 0;
  virtual void printMatchInfo(const char *) = 0;
  virtual void printDecomp(const char *) = 0;

  virtual void printGeometryEnsight(const char *) {
    printf("Error: Ensight output not defined\n"); exit(1);
  }

  void printNodes(int, BinFileHandler &, Point3D *);
  void printNodes(int, EnsightFileHandler &, Point3D *);

  void printInterfaceInfo(BinFileHandler &, int, int *, Connectivity *, MapVec &);
  void printBC(BinFileHandler &, int);

};

//-------------------------------------------------------------------

class FluidDomain : public Domain {

#define MIN_TYPE_BC -6
#define MAX_TYPE_BC 12

  bool* type_active;
  const char** type_name;

  int numFace;
  Face *faces;

  int numMatchPts;
  int *matchPts;
  double (*gapVec)[3];

  MapVec *gl2ClFaceMap;

public:

  FluidDomain(int);
  ~FluidDomain();

  void getGeometry(const char *);
  void getGeometryXpost(const char *);
  void getGeometryFieldView(const char *);
  void getGeometrySinus(const char *);
  Connectivity *getBinaryGeometry(const char *); 
  void getMatchInfo(const char *name);
  void getBinaryMatchInfo(const char *name);

  void makeConnectivity(Connectivity *);
  void renumberNodes();

  void printGeometry(const char *);
  void printGeometryEnsight(const char *);

  int  *printFaces(int, BinFileHandler &, int &, Connectivity *, MapVec &);
  void printMatchInfo(const char *);
  void printDecomp(const char *);

};

//-------------------------------------------------------------------

class StructureDomain : public Domain  {

private:

  BlockAlloc ba;

  //Match Data
  int *matchEle;
  double (*matchCoord)[2];  
  double (*gapVec)[3];  
  int numMatchData;

  // Material Properties
  int numprops;              // number of properties
  StructProp *sProps;        // struct prop. ONLY for structure mesh

  int na, namax;             // number of attributes, max # of attributes
  Attrib *attrib;            // set of attributes ONLY for structure

  int numlayInfo;            // number of Layer Sets
  ResizeArray<LayInfo *>  layInfo;  // Layer information for composites

  int numCoefData;		// number of Coefficient Data
  ResizeArray<CoefData *> coefData; // stiffness coef. data for composites

  int numCFrames;                // number of composite frames
  ResizeArray<double *> cframes; // composite frames

  EFrameData *firstEFData;   // Element frame data structure (beams)

/* moved to Domain class
  // Boundary Conditions Data
  int numDirichlet;          // number of dirichlet bc
  BCond* dbc;                // set of those dirichlet bc
  int *numDbcPerClus;
  int **dbcPerClus;

  int numNeuman;             // number of Neuman bc
  BCond* nbc;                // set of Neuman bc
  int *numNbcPerClus;
  int **nbcPerClus;

  int numConvBC;             // number of convective bc
  BCond* cvbc;               // set of convective bc
  int *numCvbcPerClus;
  int **cvbcPerClus;

  int numIDis;               // number of Initial displacements
  BCond *iDis;               // set of those initial displacements
  int *numIDisPerClus;
  int **iDisPerClus;

  int numIDis6;              // num of Initial displacements (6 column)
  BCond* iDis6;              // set of those intitial displacements
  int *numIDis6PerClus;
  int **iDis6PerClus;

  int numIVel;               // number of initial velocities
  BCond *iVel;               // set of those initial velocities
  int *numIvelPerClus;
  int **iVelPerClus;

  int numITemp;              // number of initial temperatures
  BCond *iTemp;              // set of those intitial temperatures
  int *numItempPerClus;
  int **iTempPerClus;
*/

  //int numLMPC;               // number of Linear Multi-Point Constraints
  //ResizeArray<LMPCons *> lmpc;       // set of those LMPC
                                                                                                             
  //int numSommer;             // number of Sommerfeld bc
  //ResizeArray<SommerElement *> sbc; // set of Sommerfeld bc

public:
  
  StructureDomain(int);
  ~StructureDomain();

  // Geometry Functions
  void getGeometry(const char *);
  Connectivity *getBinaryGeometry(const char *) { return 0; };
  //void renumberBC(MapVec &, int);
  //void initBCData();

  // Decomposition functions
  void getMatchInfo(const char *name);
  void getBinaryMatchInfo(const char *name);

  // Output function
  void renumberNodes();
  void makeConnectivity(Connectivity *);
  void printGlobalConnectivity(const char *); 
  void printDecomp(const char *);
  void printGeometry(const char *);
  void printAttr(int, BinFileHandler &, int, MapVec &);
  void printStructProp(BinFileHandler &);
  void printCFrames(double **, BinFileHandler &);
  void printEFrames(BinFileHandler &, MapVec &);
  void printCompLayers(BinFileHandler &);
  void printCoefData(BinFileHandler &);
  void printMatchInfo(const char *);
  //void printBC(BinFileHandler &, int);

  // Parser support functions
  void addNode(int, double[3]);
  void elemAdd(int eNum, int eType, int nn, int *n);
  int  addMat(int, StructProp &);  // adds a set of material properties
  int  setAttrib(int, int, int = -1, int = -1, double = 0.0);
  int  setFrame(int, double *);  // adds an Eframe for beams
  int  addCFrame(int, double *); // adds a composite frame for composites
  int  setDirichlet(int, BCond *);  // adds a Dirichlet bc
  int  setNeuman(int, BCond *);
  int  setConvBC(int,BCond *);
  int  setIDis6(int, BCond *);
  int  setIDis(int, BCond *);
  int  addLay(int, LayInfo *);
  int  addCoefInfo(int, CoefData &);

  
};
#endif
