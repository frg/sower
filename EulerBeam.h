#ifndef _EULERBEAM_H_
#define _EULERBEAM_H_

#include        <Element.h>


class EulerBeam : public Element {

        EFrame *elemframe;
public:

	EulerBeam(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 6;}

        void binWriteNodes(BinFileHandler &);
};
#endif
