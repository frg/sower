#include <cstdio>
#include <cstdlib>
#include <cmath>

#include <Element.h>
#include <BinFileHandler.h>

//---------------------------------------------------------------

void Element::binWriteNodes(BinFileHandler &outfile)  {

  int numNode = numNodes();
  int *nodePtr = nodes();

  outfile.write(nodePtr, numNode);
}

//---------------------------------------------------------------

void Element::renumberNodes(MapVec &gl2Cl)  {

  int numNodes = this->numNodes();

  for (int iNode = 0; iNode < numNodes; iNode++)  {

    // check if mapping exists
    /*
    if (gl2Cl.find(nn[iNode]) == gl2Cl.end()) {  
      fprintf(stderr, "*** Error: Mapping does not exist for Global Node: %d\n", nn[iNode]);
      fprintf(stderr, "... Aborting\n");
      exit (-1);
    }
    */
    nn[iNode] = gl2Cl[ nn[iNode] ];

  }

}

//---------------------------------------------------------------
