#ifndef	_TIMOSHENKOBEAM_H_
#define	_TIMOSHENKOBEAM_H_

#include        <Element.h>


class TimoshenkoBeam : public Element {
        EFrame *elemframe;


  	double* iniOr;	
	
public:
        TimoshenkoBeam(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 7; }

	void binWriteNodes(BinFileHandler &);
};
#endif
