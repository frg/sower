#ifndef _TWENTYNODEBRICK_H_
#define _TWENTYNODEBRICK_H_

#include <Element.h>

class Brick20: public Element {

public:
        Brick20(int*);

        int              numNodes();
        int*             nodes(int * = 0);
        int getType()  { return 72; }

        void binWriteNodes(BinFileHandler &);
};

#endif
