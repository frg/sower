%x incl

%{
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include "aux.h"
#include "parser.hpp"
#include <stack>

int line_num = 1;

// used for include
stack<YY_BUFFER_STATE> includedFiles;
stack<int> lineNumber;	// for fancy error message

%}

%e 40000
%p 10000
%a 40000
%n 10000
%o 30000



DIGIT [0-9]
HEXDIGIT [0-9A-Fa-f]
SPACES [ \t]+
NAME [a-zA-Z_][a-zA-Z_0-9]*
TRAIL [a-zA-Z]*
%%

"INCLUDE" 		{ BEGIN(incl); 	} // go to include state 
<incl>[ \t]* 
<incl>[^ \t\n]+		{
				// clean up !
				int c = yyinput();
				while(c == '\t' || c == ' ')
					c = yyinput();
				if(c != '\n')
					yyunput(c,yytext_ptr);
				string fname = string(yytext);
				if(fname.length() > 0)
				{
					if(fname[0]=='"' && fname[ fname.length()-1 ]=='"' && fname.length()> 2)
						fname = fname.substr(1,fname.length()-2);
						
					FILE * f = fopen(fname.c_str(),"r");
					if(f!=NULL)
					{	// open the file and substitute it as the current buffer
						std::cout << " ... Including file " << fname << endl;
						includedFiles.push(YY_CURRENT_BUFFER);
						lineNumber.push(line_num);
						line_num = 1;
						yy_switch_to_buffer( yy_create_buffer(f,YY_BUF_SIZE) );
					}
					else
					{
						std::cout << " **************************************************** "<< endl 
							  << " ** ERROR :  Could'nt open file " << fname << endl
							  << " **************************************************** "<< endl 
;
					}
					BEGIN(INITIAL); // go back to parsing
				}	
		    }
<<EOF>>	    {
			if(includedFiles.size()==0)
			{	// end of everything
				yyterminate();
			}
			else
			{	// switch to previous buffer
		    	   yy_delete_buffer( YY_CURRENT_BUFFER );
            		   yy_switch_to_buffer( includedFiles.top());
			   line_num = lineNumber.top()+1;
			   lineNumber.pop();
			   includedFiles.pop();
			}
	    }

"NODE"[a-zA-Z]*     { return NODE;    }

"ZERO"[a-zA-Z]*     { return ZERO;    }

"READMODE"[a-zA-Z]* { return READMODE; }

"PRES"[a-zA-Z]*     { return PRESSURE; }

"FACE"|"face"       { return FACE; }

"BUCK"[a-zA-Z]*     { return BUCKLE; }

"CONT"[a-zA-Z]*     { return CONTROL;  }

"DECOMP"[a-zA-Z]*|"decomp"[a-zA-Z]*   { return DECOMPOSE; }

"nsubs"|"NSUBS"                    { return NSUBS; }

"deter"|"DETER"                    { return DETER; }

"STRU"[a-zA-Z]*     { return OPTIMIZATION;  }

"TOPO"[a-zA-Z]*     { return TOPOLOGY; }

"TOPF"[a-zA-Z]*     { return TOPFILE;  }

"ATTR"[a-zA-Z]*     { return ATTRIBUTES; }

"THETA"|"theta"     { return THETA; }

"IDENTITY"          { return IDENTITY; }

"EFRA"[a-zA-Z]*     { return EFRAMES; }

"IDISP"[a-zA-Z]*    { return IDIS;  }

"IDISP6"[a-zA-Z]* { return IDIS6; }

"GEPS"[a-zA-Z]*   { return GEPS; }

"IVEL"[a-zA-Z]* { return IVEL; }

"IACC"[a-zA-Z]* { return IACC; }

"ITEM"[a-zA-Z]* { return ITEMP; }

"TEMP"[a-zA-Z]* { return TEMP; }

"FLUX"[a-zA-Z]* { return FLUX; } 

"END"  { return END;  }

"LOAD"  { return LOAD; }

"USE"   { return USE;  }

"SENS"[a-zA-Z]* { return SENSORS; }

"ACTU"[a-zA-Z]* { return ACTUATORS; }

"MATE"[a-zA-Z]* { return MATERIALS; }

"DISP"[a-zA-Z]* { return DISP; }

"THRU"[a-zA-Z]*|"thru"[a-zA-Z]*    { return THRU; }

"STEP"[a-zA-Z]*|"step"[a-zA-Z]*    { return STEP; }

"LMPC"[a-zA-Z]* { 
     fprintf(stderr," *** WARNING: LMPCs implemented for FETI Solver only.\n"); 
     return LMPC; }

"ATDARB"[a-zA-Z]* { return ATDARB; }

"ATDNEU"[a-zA-Z]* { return ATDNEU; }

"ATDDNB"[a-zA-Z]* { return ATDDNB; }

"ATDDIR"[a-zA-Z]* { return ATDDIR; }

"ATDROB"[a-zA-Z]* { return ATDROB; }

"HELMHOLTZ"|"helmholtz"            { return HELMHOLTZ; }

"k"|"K"                            { return WAVENUMBER; }

"bgtl"|"BGTL"                      { return BGTL; }

"incidence"|"INCIDENCE"            { return INCIDENCE; }

"HDIR"[a-zA-Z]* { return HDIRICHLET; }

"HSOM"[a-zA-Z]* { return HSOMMERFELD; }

"FACO"[a-zA-Z]* { return FACOUSTICS; }

"FORC"[a-zA-Z]* { return FORCE; }

"HNEU"[a-zA-Z]* { return HNEUMAN; }

"HNBO"[a-zA-Z]*|"hnbo"[a-zA-Z]*    { return HNBO; }

"HDNB"[a-zA-Z]*|"hdnb"[a-zA-Z]*    { return HNBO; }

"HSCB"[a-zA-Z]*|"hscb"[a-zA-Z]*    { return HNBO; }
 
"HARB"[a-zA-Z]*|"harb"[a-zA-Z]*    { return ELHSOMMERFELD; }

"COND"[a-zA-Z]* { return CONDITION; }

"MASS"[a-zA-Z]*|"mass"[a-zA-Z]* { return MASS; }

"STAT"[a-zA-Z]* { return STATS; }

"AXIHDIR" { return AXIHDIR; }

"AXIHNEU" { return AXIHNEU; }

"AXINUMMODES" { return AXINUMMODES; }

"AXINUMSLICES" { return AXINUMSLICES; }

"AXIHSOMMER" { return AXIHSOMMER; }

"AXIMPC" {return AXIMPC; }

"ELSCATTERER" { return ELSCATTERER; }

"NOSCATTERER" { return NOSCATTERER; }

"FFP" {return FFP; }

"numcgm" { return NUMCGM; }
"interfacelumped" { return INTERFACELUMPED; }



"PCG" { yylval.ival = 1; return ITERTYPE; }
"pcg" { yylval.ival = 1; return ITERTYPE; }
"bcg" { yylval.ival = 2; return ITERTYPE; }
"BCG" { yylval.ival = 2; return ITERTYPE; }
"cr"  { yylval.ival = 3; return ITERTYPE; }

"HFET" { return HFETI; }

"FETI"|"feti" { return FETI;  }

"OLD"  { yylval.ival = 0; return FETI2TYPE; }
"NEW"  { yylval.ival = 1; return FETI2TYPE; }
"DP"|"dp"   { return DP; }
"DPH"|"dph"                        { return DPH; }

"noco"[a-zA-Z]*      { return NOCOARSE;  }
"proj"[a-zA-Z]*      { return PROJ;      }

"local_solver"       { return LOCALSOLVER;  }
"coarse_solver"      { return COARSESOLVER; }
"outerloop"|"OUTERLOOP"            { return OUTERLOOP; }

"skyl"[a-zA-Z]*      { yylval.ival = 0; return SOLVERTYPE; }
"spar"[a-zA-Z]*      { yylval.ival = 1; return SOLVERTYPE; }
"sgisparse"[a-zA-Z]* { yylval.ival = 2; return SOLVERTYPE; }
"sgisky"[a-zA-Z]*    { yylval.ival = 3; return SOLVERTYPE; }
"fron"[a-zA-Z]*      { yylval.ival = 5; return SOLVERTYPE; }
"bloc"[a-zA-Z]*      { yylval.ival = 6; return SOLVERTYPE; }
"para"[a-zA-Z]*      { yylval.ival = 7; return SOLVERTYPE; }

"CG"|"cg"                          { yylval.ival = 0; return OUTERLOOPTYPE; }
"GMRES"|"gmres"                    { yylval.ival = 1; return OUTERLOOPTYPE; }
"GCR"|"gcr"                        { yylval.ival = 2; return OUTERLOOPTYPE; }
"CGAL"|"cgal"                      { yylval.ival = 4; return OUTERLOOPTYPE; }

"kryp"[a-zA-Z]*      { return NLPREC;    }
"tolf"[a-zA-Z]*      { return TOLFETI;   }
"global_rbm_tol"     { return GLOBALTOL; }
"maxo"[a-zA-Z]*      { return MAXORTHO;  }
"vers"[a-zA-Z]*      { return VERSION;   }
"USDF"[a-zA-Z]*      { return USERDEFINEFORCE; }
"USDD"[a-zA-Z]*      { return USERDEFINEDISP;  }
"printMat"[a-zA-Z]*  { return PRINTMATLAB;    }

"DIRE"[a-zA-Z]*      { return DIRECT; }
"dire"[a-zA-Z]*      { return DIRECT; }

"RENU"[a-zA-Z]*   { return RENUM; }

"DYNA"[a-zA-Z]*   { return DYNAM; }
"rbmf"[a-zA-Z]*   { return RBMFILTER; }
"RESTART" { return RESTART; }

"CONV"[a-zA-Z]*   { return CONVECTION; }

"newm"[a-zA-Z]* { return NEWMARK; }

"qsta"[a-zA-Z]* { return QSTATIC; }

"precn"o[a-zA-Z]* { return PRECNO; }

"stea"[a-zA-Z]* { return STEADYSTATE; }

"tolp"[a-zA-Z]* { return TOLPCG; }

"tolb"[a-zA-Z]* { return TOLPCG; }

"tolc"[a-zA-Z]* { return TOLPCG; }

"maxi"[a-zA-Z]*   { return MAXITR; }

"nsbs"[a-zA-Z]* { return NSBSPV; }

"neig"[a-zA-Z]* { return NEIGPA; }

"tole"[a-zA-Z]* { return TOLEIG; }

"tolj"[a-zA-Z]* { return TOLJAC; }

"damp"[a-zA-Z]* { return DAMPING; }

"time" { return TIME; }

"heat" { return HEAT; }

"mech" { return NEWMARK; }

"acou" { return ACOUSTIC; }

"OUTPUT"  { return OUTPUT; }

"OUTPUT6" { return OUTPUT6; }

"EIGE"[a-zA-Z]*  { return EIGEN; }

"subs"[a-zA-Z]*     { return SUBSPACE;  }

"NONL"[a-zA-Z]* { return NL; }

"FITA"[a-zA-Z]* { return FITALG; }

"nlto"[a-zA-Z]* { return NLTOL; }

"maxv"[a-zA-Z]* { return MAXVEC; }

"arcl"[a-zA-Z]* { return ARCLENGTH; }

"dlam"[a-zA-Z]* { return DLAMBDA; }

"lfac"[a-zA-Z]* { return LFACTOR; }

"rebu"[a-zA-Z]* { return REBUILD; }
"REBU"[a-zA-Z]* { return REBUILD; }

"tang"[a-zA-Z]* { return TANGENT; }
"preco"[a-zA-Z]* { return PRECONDITIONER; }

"DIMA"[a-zA-Z]* { return DIMASS; }

"COMP"[a-zA-Z]* { return COMPOSITE; }

"COEF" { return COEF; }

"CFRA"[a-zA-Z]* { return CFRAMES; }

"LAYC" { return LAYC; }

"LAYN" { return LAYN; }

"TRBM"[a-zA-Z]* { return TRBM; }

"GRBM"[a-zA-Z]* { return GRBM; }

"GRAV"[a-zA-Z]* { return GRAVITY; }

"MFTT" { return MFTT; }

"REOR"[a-zA-Z]* { return REORTHO; }

"scal"[a-zA-Z]* { return SCALING; }
"stif"[a-zA-Z]* { yylval.ival = 1; return SCALINGTYPE; }
"topo"[a-zA-Z]* { yylval.ival = 2; return SCALINGTYPE; }

"sloa"[a-zA-Z]* { yylval.ival = 1; return RENUMBERID; }
"rcm"[a-zA-Z]*  { yylval.ival = 2; return RENUMBERID; }

"corn"[a-zA-Z]*      { return CORNER; }
"sixdof"   { yylval.ival = 0; return CORNERTYPE; }
"threedof" { yylval.ival = 1; return CORNERTYPE; }
"noEnd6"   { yylval.ival = 2; return CORNERTYPE; }
"noEnd3"   { yylval.ival = 3; return CORNERTYPE; }
"be6"      { yylval.ival = 0; return CORNERTYPE; }
"be3"      { yylval.ival = 1; return CORNERTYPE; }
"cp6"      { yylval.ival = 2; return CORNERTYPE; }
"cp3"      { yylval.ival = 3; return CORNERTYPE; }

"augment" { return AUGMENT; }
"none"    { yylval.ival = 0; return AUGMENTTYPE;}
"Gs"      { yylval.ival = 1; return AUGMENTTYPE;}
"EdgeGs"  { yylval.ival = 2; return AUGMENTTYPE;}
"EdgeWs"|"edgews"|"EDGEWS"         { return EDGEWS; }
"solid"|"SOLID"                    { yylval.ival = 0; return WAVETYPE;}
"shell"|"SHELL"                    { yylval.ival = 1; return WAVETYPE;}
"fluid"|"FLUID"                    { yylval.ival = 2; return WAVETYPE;}
"orthotol"|"ORTHOTOL"              { return ORTHOTOL; }
"spacedim"|"SPACEDIM"              { return SPACEDIMENSION; }

"CONMAT"|"conmat"                  { return CONSTRMAT; }
"MULTIPLIERS"|"multipliers"        { return MULTIPLIERS; }
"PENALTY"|"penalty"                { return PENALTY; }

"AERO"     { return AERO; }
"AEROHEAT" { return AEROHEAT; }
"THERMLOAD" { return THERMLOAD; }
"MECHHEAT" { return MECHHEAT; }

"COLL"[a-zA-Z]* { yylval.ival = 1; return COLLOCATEDTYPE; }
"coll"[a-zA-Z]* { yylval.ival = 1; return COLLOCATEDTYPE; }
"NONC"[a-zA-Z]* { yylval.ival = 0; return COLLOCATEDTYPE; }
"nonc"[a-zA-Z]* { yylval.ival = 0; return COLLOCATEDTYPE; }

"A0" { yylval.ival = 0; return AEROTYPE; }
"PP" { yylval.ival = 1; return AEROTYPE; }
"A4" { yylval.ival = 4; return AEROTYPE; }
"A5" { yylval.ival = 5; return AEROTYPE; }
"A6" { yylval.ival = 6; return AEROTYPE; }
"A7" { yylval.ival = 7; return AEROTYPE; }
"MD" { yylval.ival = 8; return AEROTYPE; }

"nodal"[a-zA-Z]*   { yylval.ival =  1; return AVERAGED; }
"element"[a-zA-Z]* { yylval.ival =  0; return AVERAGED; }

"upper"[a-zA-Z]*  { yylval.ival = 1; return SURFACE; }
"median"[a-zA-Z]* { yylval.ival = 2; return SURFACE; }
"lower"[a-zA-Z]*  { yylval.ival = 3; return SURFACE; }

"gdisplac" {yylval.ival = OutputInfo::Displacement; return STRESSID; }
"gtempera" {yylval.ival = OutputInfo::Temperature;  return STRESSID; }
"stressxx" {yylval.ival = OutputInfo::StressXX;     return STRESSID; }
"stressyy" {yylval.ival = OutputInfo::StressYY;     return STRESSID; }
"stresszz" {yylval.ival = OutputInfo::StressZZ;     return STRESSID; }
"stressxy" {yylval.ival = OutputInfo::StressXY;     return STRESSID; }
"stressxz" {yylval.ival = OutputInfo::StressXZ;     return STRESSID; }
"stresszx" {yylval.ival = OutputInfo::StressXZ;     return STRESSID; }
"stressyz" {yylval.ival = OutputInfo::StressYZ;     return STRESSID; }
"strainxx" {yylval.ival = OutputInfo::StrainXX;     return STRESSID; }
"strainyy" {yylval.ival = OutputInfo::StrainYY;     return STRESSID; }
"strainzz" {yylval.ival = OutputInfo::StrainZZ;     return STRESSID; }
"strainxy" {yylval.ival = OutputInfo::StrainXY;     return STRESSID; }
"strainzx" {yylval.ival = OutputInfo::StrainXZ;     return STRESSID; }
"strainxz" {yylval.ival = OutputInfo::StrainXZ;     return STRESSID; }
"strainyz" {yylval.ival = OutputInfo::StrainYZ;     return STRESSID; }
"heatflxx" {yylval.ival = OutputInfo::HeatFlXX;     return STRESSID; }
"heatflxy" {yylval.ival = OutputInfo::HeatFlXY;     return STRESSID; }
"heatflxz" {yylval.ival = OutputInfo::HeatFlXZ;     return STRESSID; }
"grdtempx" {yylval.ival = OutputInfo::GrdTempX;     return STRESSID; }
"grdtempy" {yylval.ival = OutputInfo::GrdTempY;     return STRESSID; }
"grdtempz" {yylval.ival = OutputInfo::GrdTempZ;     return STRESSID; }
"stressvm" {yylval.ival = OutputInfo::StressVM;     return STRESSID; }
"inxforce" {yylval.ival = OutputInfo::InXForce;     return STRESSID; }
"inyforce" {yylval.ival = OutputInfo::InYForce;     return STRESSID; }
"inzforce" {yylval.ival = OutputInfo::InZForce;     return STRESSID; }
"axmoment" {yylval.ival = OutputInfo::AXMoment;     return STRESSID; }
"aymoment" {yylval.ival = OutputInfo::AYMoment;     return STRESSID; }
"azmoment" {yylval.ival = OutputInfo::AZMoment;     return STRESSID; }
"energies" {yylval.ival = OutputInfo::Energies;     return STRESSID; }
"raerofor" {yylval.ival = OutputInfo::AeroForce;    return STRESSID; }
"geigenpa" {yylval.ival = OutputInfo::EigenPair;    return STRESSID; }
"strainvm" {yylval.ival = OutputInfo::StrainVM;     return STRESSID; }
"ghelmhol"|"gacoustd"|"pressure" {yylval.ival = OutputInfo::Helmholtz;    return STRESSID; }
"farfield" {yylval.ival = OutputInfo::Farfield;    return STRESSID; }
"gvelocit" {yylval.ival = OutputInfo::Velocity;    return STRESSID; }

"MATLAW"|"matlaw"                  { return MATSPEC; }
"MATUSAGE"|"matusage"              { return MATUSAGE; }

"bili"[a-zA-Z]*|"BILI"[a-zA-Z]*|"Bili"[a-zA-Z]* { return MATTYPE; }
"fini"[a-zA-Z]*|"FINI"[a-zA-Z]*|"Fini"[a-zA-Z]* { return MATTYPE; }
"logs"[a-zA-Z]*|"LOGS"[a-zA-Z]*|"LogS"[a-zA-Z]* { return MATTYPE; }
"line"[a-zA-Z]*|"LINE"[a-zA-Z]*|"Line"[a-zA-Z]* { return MATTYPE; }
"hype"[a-zA-Z]*|"HYPE"[a-zA-Z]*|"Hype"[a-zA-Z]* { return MATTYPE; }
"isot"[a-zA-Z]*|"ISOT"[a-zA-Z]*|"Isot"[a-zA-Z]* { return MATTYPE; }
"visc"[a-zA-Z]*|"VISC"[a-zA-Z]*|"Visc"[a-zA-Z]* { return MATTYPE; }
"PlaneStress"[a-zA-Z]*                          { return MATTYPE; }
"stve"[a-zA-Z]*|"STVE"[a-zA-Z]*|"StVe"[a-zA-Z]* { return MATTYPE; }
"henc"[a-zA-Z]*|"HENC"[a-zA-Z]*|"Henc"[a-zA-Z]* { return MATTYPE; }
"neoh"[a-zA-Z]*|"NEOH"[a-zA-Z]*|"NeoH"[a-zA-Z]* { return MATTYPE; }
"moon"[a-zA-Z]*|"MOON"[a-zA-Z]*|"Moon"[a-zA-Z]* { return MATTYPE; }
"ogde"[a-zA-Z]*|"OGDE"[a-zA-Z]*|"Ogde"[a-zA-Z]* { return MATTYPE; }
"simo"[a-zA-Z]*|"SIMO"[a-zA-Z]*|"Simo"[a-zA-Z]* { return MATTYPE; }
"hypo"[a-zA-Z]*|"HYPO"[a-zA-Z]*|"Hypo"[a-zA-Z]* { return MATTYPE; }
"elas"[a-zA-Z]*|"ELAS"[a-zA-Z]*|"Elas"[a-zA-Z]* { return MATTYPE; }
"j2"[a-zA-Z]*|"J2"[a-zA-Z]*                     { return MATTYPE; }
"kk"|"KK"|"kk1"|"KK1"                           { return MATTYPE; }
"kkexp"|"KKEXP"|"kk2"|"KK2"                     { return MATTYPE; }
"kkexp2"|"KKEXP2"                               { return MATTYPE; }

[A-Za-z][A-Za-z0-9_.]*  { yylval.strval = strdup(yytext); return FNAME; }

"\""[A-Za-z0-9_./]*"\"" { yytext[yyleng-1] = 0; yylval.strval = strdup(yytext+1);
                           return FNAME; }

"\n" { line_num++; /* Count line numbers */ return NewLine; }

{SPACES} /* Just discard spaces */

[+-]?{DIGIT}+ {yylval.ival = atoi(yytext) ; return IntConstant ; }

[+-]?{DIGIT}+"."{DIGIT}* |
[+-]?"."{DIGIT}+ |
[+-]?{DIGIT}+"."{DIGIT}*[eE][+-]?{DIGIT}+ |
[+-]?{DIGIT}+[eE][+-]?{DIGIT}+ |
[+-]?"."{DIGIT}+[eE][+-]?{DIGIT}+ {
       yylval.fval = atof(yytext) ; return DblConstant ; }

[+-]?{DIGIT}+"."{DIGIT}*[dD][+-]?{DIGIT}+ |
[+-]?{DIGIT}+[dD][+-]?{DIGIT}+ |
[+-]?"."{DIGIT}+[dD][+-]?{DIGIT}+ {
       int il;
       for(il = 0; yytext[il] != 'd' && yytext[il] != 'D';) ++il;
       yytext[il] = 'e'; yylval.fval = atof(yytext) ; return DblConstant ; }

"*" { while(yyinput() != '\n') ; line_num++; }

%%
void
yyerror(const char  *)
{
 fprintf(stderr,"Syntax Error near line %d : %s\n", line_num,yytext);
}

int yywrap()
{
 return 1;
}
