#ifndef _HELMISOPARAMTETRA_H_
#define _HELMISOPARAMTETRA_H_
                                                                                                 
#include <Element.h>
                                                                                                 
class HelmIsoParamTetra: public Element {
                                                                                                 
        double *C;
        int nNodes;
public:
        HelmIsoParamTetra(int,int*);
                                                                                                 
        int  numNodes();
        int* nodes(int * = 0);
        int getType()  { return 96; }
                                                                                                 
        void binWriteNodes(BinFileHandler &);
                                                                                                 
};
#endif
 
