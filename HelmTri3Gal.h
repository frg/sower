#ifndef _HELMTRI3GAL_H_
#define _HELMTRI3GAL_H_

#include <Element.h>

class HelmTri3Gal: public Element {

public:
	HelmTri3Gal(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 35; }

	void binWriteNodes(BinFileHandler &);
};

#endif

