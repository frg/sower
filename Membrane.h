#ifndef _MEMBRANE_H_
#define _MEMBRANE_H_

#include <Element.h>

class Membrane : public Element {

public:
	Membrane(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 19; }

	void binWriteNodes(BinFileHandler &);
};
#endif

