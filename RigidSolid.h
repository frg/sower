#ifndef _RIGIDSOLID_H_
#define _RIGIDSOLID_H_

#include	<Element.h>


class RigidSolid : public Element {

        int n;

public:
	RigidSolid(int, int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 74; }

	void	binWriteNodes(BinFileHandler &);
};
#endif

