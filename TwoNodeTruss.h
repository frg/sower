#ifndef _TWONODETRUSS_H_
#define _TWONODETRUSS_H_

#include <Element.h>

class TwoNodeTruss : public Element {


public:

	TwoNodeTruss(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 1; }

	void binWriteNodes(BinFileHandler &);
};
#endif
