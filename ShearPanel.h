#ifndef _SHEARPANEL_H_
#define _SHEARPANEL_H_

#include <Element.h>

class ShearPanel: public Element {

public:
	ShearPanel(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 18; }

        void binWriteNodes(BinFileHandler &);

};
#endif

