#ifndef _TETRAHELMGLS_H_
#define _TETRAHELMGLS_H_

#include <Element.h>

class TetraHelmGLS: public Element {

public:
	TetraHelmGLS(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 41; }

        void	binWriteNodes(BinFileHandler &);
	
};
#endif

