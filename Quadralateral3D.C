#include"Quadralateral3D.h"


Quadralateral3D::Quadralateral3D(int inSize, int* inNodes){
	assert(inSize==4);

	nn = new int[4];

	nn[0]=inNodes[0];
	nn[1]=inNodes[1];
	nn[2]=inNodes[2];
	nn[3]=inNodes[3];
}
int
Quadralateral3D::getNode(int position){
	assert(0<=position);
	assert(position<=3);
	return nn[position];
}

/*
void
Quadralateral3D::match(ResizeArray<Point3D*>& allPoints,
const Point3D& matchPoint, const double tol, MatchInfo& info)const {

	Point3D x1=*(allPoints[nodes[0]]);
	Point3D	x2=*(allPoints[nodes[1]]);
	Point3D x3=*(allPoints[nodes[2]]);
	Point3D x4=*(allPoints[nodes[3]]);
	Point3D d1=x1+x3-x4-x2;
	Point3D d2=x2-x1;
	Point3D d3=x4-x1;
	if(dotp(d1,d1)>=1.0E-8){
		Point3D cp=crossp(d2,d3);
		Point3D n=cp/norm(cp);
		double alpha=dotp(x1-matchPoint,n);
		info.point=matchPoint+alpha*n;
		Point3D mmp=info.point-x1;	
		cp=crossp(d1,d2);
		info.eta=dotp(mmp,cp)/dotp(d3,cp);
		cp=crossp(d1,d3);
		info.xi=dotp(mmp,cp)/dotp(d2,cp);
		info.success=0;
		if((-tol<=info.xi)&&(info.xi<=1.0+tol)){
			if((-tol<=info.eta)&&(info.eta<=1.0+tol)){
				info.dist=abs(alpha);
				info.success=1;
			}
		}
	}
	else{
		double c,s,t;
		double A[2][2],b[2];
		Point3D mmp=matchPoint-x1;
		A[0][0]=dotp(d2,d2);
		A[1][0]=A[0][1]=dotp(d2,d3);
		A[1][1]=dotp(d1,d1);
		b[0]=dotp(mmp,d1);
		b[1]=dotp(mmp,d2);
		if(abs(A[1][0])<1.0E-8){
			c=1.0;
			s=0.0;
		}
		else {
			if(abs(A[1][0])>abs(A[0][0])){
				t=-A[0][0]/A[1][0];
				s=1.0/(sqrt(1+t*t));
				c=s*t;
			}
			else{
				t=-A[1][0]/A[0][0];
				c=1.0/(sqrt(1+t*t));
				s=c*t;
			}
		}
		double t1=A[0][1];
		A[0][0]=c*A[0][0]-s*A[1][0];
		A[0][1]=c*A[0][1]-s*A[1][1];
		A[1][1]=s*t1+c*A[1][1];
		t1=b[0];
		b[0]=c*t1-s*b[1];
		b[1]=s*t1+c*b[1];
		info.eta=b[1]/A[1][1];
		info.xi=(b[0]-info.eta*A[0][1])/A[0][0];
		info.success=0;
  	if((-tol<=info.xi)&&(info.xi<=1.0+tol)){
			if((-tol<=info.eta)&&(info.eta<=1.0+tol)){
				info.point=info.eta*info.xi*d1+info.xi*d2+info.eta*d3;
				info.dist=norm(info.point-matchPoint);
				info.success=1;
			}
		}
	}
}
//In it's initial form the equation containing the natural coord's
//of z has the form z=x1+xi*eta*d1+xi*d2+eta*d3.   We need to 
//overcome the non-linear nature of this problem in order
//to solve directly.  To this end we find a system of two linear 
//equations by finding two vectors orthoganal to first d1 and d2
//and then d1 and d3.  If we take the dot product of both sides
//with respect to these two vectors, the desired linear system 
//arrises.  In addition we have succeeded in decoupling the
//problem, so it can be solved directly at this point.
//	However if <d1,d1>=||d1||^2=0 (meaning the quad is
//a parrallelogram) then this method will not work.  Fortunately
//in this case d1=0 so the original problem is a linear system
//itself.  Written in the standard form Ax=b where x=[xi,eta]^T,
//A=[d2,d3], and b=z-x1=matchPoint+alpha*n the system seems
//to be over-determined (dim(A)=3X2).  Fortunately since the 
//points of our quad are co-planar, the rows of A are not linear
//independant.  To overcome this we reformulate the problem as a 2x2
//linear system by taking the dot product of both sides with respect
//to d2 and d3.  It should be noted that n=k*(d2xd3) so <n,d2>=<n,d3>=0.
//This means that we can replace z by matchPoint in our calculation. 
//The resulting system may have a large condition number so a givens
//transformation is used to solve.

*/

//---------------------------------------------------------------
void
Quadralateral3D::binWriteNodes(BinFileHandler &outfile) {

  int type = 0;
  outfile.write(&type, 1);
  outfile.write(nn, 4);

}
