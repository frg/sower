#ifndef _TOOLS_
#define _TOOLS_

/*
template<class T>
inline T min(T a,T b){return(a<b?a:b);}

template<class T>
inline T max(T a,T b){return(a>b?a:b);}
*/

template<class T>
inline T abs(T a){return max(a,-a);}
#include <algorithm>

using std::min;
using std::max;

#endif
