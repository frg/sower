#ifndef	_ELEMENT_H_
#define	_ELEMENT_H_

#include "BinFileHandler.h"
#include "EnsightFileHandler.h"

#include "MappingVector.h"

//----------------------------------------------------------------

// Boundary Condition Structure
struct BCond {
 int    nnum;   // node number
 int    dofnum; // dof number
 double val;    // value of bc
 void setData(int _nnum, int _dofnum, double _val) { nnum = _nnum; dofnum = _dofnum; val = _val; }
};

//----------------------------------------------------------------

typedef double EFrame[3][3];

// ****************************************************************
// * Class StructProp contains the defined structural properties  *
// ****************************************************************

// contains material and geometrical properties of elements

class StructProp {
  public:
        double  E;      // Elastic modulus
        double  A;      // Cross-sectional area
        double  nu;     // Poisson's ratio
        double  rho;    // Mass density per unit volume
     union {
        double  eh;     // Element thickness
        double  C1;     // Non-uniform torsion constant
        };
        double  Ixx;    // Cross-sectional moment of inertia about local x-axis
        double  Iyy;    // Cross-sectional moment of inertia about local y-axis
        double  Izz;    // Cross-sectional moment of inertia about local z-axis
     union {
        double c;       // Thermal convection coefficient
        double alphaY;  // Shear deflection constant associated to Iyy
        };
     union {
        double k;       // Heat conduction coefficient
        double alphaZ;  // Shear deflection constant associated to Izz
        };
        double Q;       // Specific heat coeffiecient
        double W;       // Thermal expansion coefficient
        double P;       // Perimeter/circumference of thermal truss/beams
        double Ta;      // Ambient temperature

        double kappaHelm; // wave number for Helmholtz proplem

        StructProp() { E=10E6; nu=0.3; A=1.0; rho=1.0; Ixx=2.0; Iyy=Izz=1.0;                       Ta=0.0; }
};

//--------------------------------------------------------------------

class Element {
protected:
  int *nn;		  // nodes for this element
  StructProp *prop;       // structural properties for this element

  int volume_id;

public:
  Element() {volume_id = -1;}
  ~Element() {}

  virtual int numNodes() = 0;
  virtual int *nodes(int *p = 0) = 0;
  virtual int getType() { return -1; }

  virtual void binWriteNodes(BinFileHandler &);

  virtual void renumberNodes(MapVec &);

    // CBM: for porous media

  virtual void setVolumeID(int i) {volume_id = i;}
  virtual int& getVolumeID() {return volume_id;}


};

#endif
