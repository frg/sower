#ifndef _TETRAHEDRAL_H_
#define _TETRAHEDRAL_H_

#include <Element.h>

class Tetrahedral: public Element {

public:
	Tetrahedral(int*);


        int              numNodes();
        int*             nodes(int * = 0);

	int getType()  { return 23; } 
        void binWriteNodes(BinFileHandler &);
};
#endif

