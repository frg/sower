#ifndef _QUADRALATERAL_
#define _QUADRALATERAL_
#include<cassert>
#include<cmath>
#include"Element.h"
#include"Point3D.h"
#include"tools.h"

//-----------------------------------------------------------


class Quadralateral3D:public Element  {

public:
	Quadralateral3D(int inSize, int* inNodes);
	int numNodes() {return 4;}
	int getNode(int position);
//	void match(ResizeArray<Point3D*>& allPoints,
//		const Point3D& matchPoint, const double tol, MatchInfo& info) const;

	int getType()  { return 0;}
        void binWriteNodes(BinFileHandler &);
};
#endif
