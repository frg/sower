//AUTHOR:Chris Saam
//FILE: Tetrahedron3D.h
//CLASS PROVIDED: Tetrahedron3D(a class containing array
//addresses of 4 Point3D types stored in an array provided
//by the user.)
//CONSTUCTORS for the Triangle3D class:
//	Tetrahedron3D()
//	Postcondition: the Tetrahedron3D object has been initialized
//    with the pointer list containing -1 (non-valid addresses).
//	Tetrahedron3D(int _n1, int _n2, int _n3, int _n4)
//	Postcondition: the Tetrahedron3D object has been initialized
//		to contain the four addresses int the argument list.
//MEMBER MEMBER OPERATOR for the Triangle3D class:
//  int operator[](const int position)
//  Precondition: 0<=position<=3.
//  Postcondition: the address stored at node[position] is accessed.
//INSERTION AND EXTRACTION OPERATORS for the Triangle3D class:
//  friend ostream& operator<<(ostream& os, const Tetrahedron3D& source)
//  Postcondition: the addresses stored in source +1 are sent to the
//    output stream with a single space between addresses and without
//    a new line character.  One is added to adjust for C's array
//		convention.
//	friend istream& operator>>(istream& is,Tetrahedron3D& source)
//  Postcondition: sources addresses are read in from the input
//    stream is and one is subtracted from each address.  The
//    input files refer to order the Point3D objects were read in
//    beginning with one (the first Point3D object read).
//    So the subtraction of one is necessary to correspond
//    to C's array addressing convention.
//VALUE SEMANTICS for the Tetrahedron3D class:
//	The assignment operator and copy constructor can be used and are
//	the defaults.
#ifndef _TETRAHEDRON3D_H_
#define _TETRAHEDRON3D_H_
#include<cstdlib>
#include<cassert>
#include<iomanip>
#include<ostream>
using std::ostream;
using std::istream;
#include"Element.h"

class Tetrahedron3D : public Element{
private:
  
public:
  Tetrahedron3D(){nn = new int[4]; nn[0]=nn[1]=nn[2]=nn[3]=-1; prop=0;}
  Tetrahedron3D(int, int, int,int);
  //int& operator[](const int position );
  int &operator[](int i) { return nn[i]; }
  friend istream& operator>>(istream& is,Tetrahedron3D& source);

  friend ostream& operator<<(ostream& os, const Tetrahedron3D& source)
    {return os<<(source.nn[0]+1)<<' '<<(source.nn[1]+1)<<' '
    <<(source.nn[2]+1)<<' '<<(source.nn[3]+1);}

  int *getElem() {return nn;}

  int numNodes() { return 4; }
  int *nodes(int * = 0);
  int getType()  { return 5; }

  void binWriteNodes(BinFileHandler &);

};  


#endif
