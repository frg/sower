#include <RotnSprlink.h>


RotnSprlink::RotnSprlink(int* nodenums)
{
	nn = new int[2];

        nn[0] = nodenums[0];
        nn[1] = nodenums[1];
}


int
RotnSprlink::numNodes()
{
        return 2;
}

int *
RotnSprlink::nodes(int *p)
{
        if(p == 0) p = new int[2];
        p[0] = nn[0];
        p[1] = nn[1];
        return p;
}

void RotnSprlink::binWriteNodes(BinFileHandler &outfile)  {

  int type = 22;
  outfile.write(&type, 1);
  outfile.write(nn, 2);
}

