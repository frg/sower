#ifndef _DEC_DOM_
#define _DEC_DOM_
#include<cassert>
#include<cmath>
#include<cstring>
#include<cstdlib>
#include<fstream>
#include <iostream>
using std::cerr;
using std::endl;

#include "MappingVector.h"

#include"Connectivity.h"
#include"Point3D.h"
#include"Tetrahedron3D.h"
#include"Triangle3D.h"
#include"aux.h"
#include"Element.h"
//#include "Decomp.h"

class GreedyDecomp;
class Face;
struct Attrib;

class Decomp3D  {
private:
  int numSub;

  int* nConnect; //a pointer an array conataining the number of
                       //of interface nodes in each subdomain

  int *numSubNeighbors;  // # of subdomain neighbors

  int** connectedDomain; // this pointer to as list of pointers
                         // is used to construct a 2D array 
                         // containing the subdomains that touch
                         // a particular subdomain.  Both nConnect
                         // and connectedDoamin are necessary to
                         // maintain the ordering used in interfNode
     
  Connectivity  *clusToSub;   // connectivity of clusters to subs
  Connectivity  *clusToNode;  // connectivity of clusters to nodes
  Connectivity  *clusToMatch;  // connectivity of clusters to nodes

  Connectivity  *nodeToSub;   // inverse connectivity of subToNode
  Connectivity  *elemToNode;  // connectivity of elements to nodes

  Connectivity  *subToNode;   // connectivity of subs to nodes
  Connectivity  *subToMatch;   // connectivity of subs to match pts
  Connectivity  *subToFace;   // connectivity of subs to Tet faces
  Connectivity  *subToElem;   // connectivity of subs to elements
  Connectivity  *subToSub;    // connectivity of subs to subs

  Connectivity  **interfNodes;// connectivity of interf nodes between sub

  //int **clusterNodeMap;   // ordered list of nodes in each cluster 
                          // in effect this is a locClusToGlobNodeMap

  int **clusNodeMapPtrs;  // ptrs to the values of # of subdomain connects
  int *numClusMapPtrs;    // # of subdomain connects in cluster i.

public:
  Decomp3D() { clusToSub = 0; };
  ~Decomp3D(){};

  void readDomain(const char* name, int);

  void getDecomp(const char* name, int);  // creates subToElem
  int getNumSub()  { return numSub; }
  int getNumDifSubConnects(int iClus) { return numClusMapPtrs[iClus]; }
  int *getClusNodeMapPtrs(int iClus) { return clusNodeMapPtrs[iClus]; }
  Connectivity **getInterfaceNodes() { return interfNodes; };
        
  // creates clusToSub, subToNode, clusToNode
  void makeConnectivity(int, int, int, Element **, Connectivity *); 
  void makeSubToFace(int, int, Face *);
  void makeSubToMatch(Connectivity *, int, int *);
  void makeSubToMatch(int, Connectivity *, int, int *);

  void getDomainConnect();
  int remapNodes(std::pair<int,int> *nodeMap, int);

  // Output functions
  void writeCpuToSubMap(int, const char*, int, int);
  //void printNodes(int, BinFileHandler &, Point3D *);
  int *printElements(int, BinFileHandler &, int &, Element **, MapVec &);
  void printElements(int iClus, 
		     EnsightFileHandler &outFileGeo, 
		     EnsightFileHandler &outFileMat, 
		     int &numClusElems, Element **elements, 
		     MapVec &gl2ClElems);

  // Connectivity access functions
  Connectivity *getClusToSub    () {return clusToSub;  }
  Connectivity *getClusToNode    () {return clusToNode;  }
  Connectivity *getClusToMatch () {return clusToMatch;  }
  Connectivity *getSubToElem    () {return subToElem;  }
  Connectivity *getSubToFace    () { return subToFace;  }
  Connectivity *getSubToNode    () {return subToNode;  }
  Connectivity *getSubToMatch    () {return subToMatch;  }
  Connectivity *getSubToSub    () {return subToSub;  }
  Connectivity *getNodeToSub    () {return nodeToSub;  }
  Connectivity **getInterfNodes    () { return interfNodes;  }
  //int **getClusterNodeMap()	{ return clusterNodeMap; }

  int *getNumNeighbors ()  { return numSubNeighbors; }
  int **getSubNeighbors ()    {return connectedDomain;}
  
};

//--------------------------------------------------------------

class NodeConnect {

public:
  int nsub;      // number of subd connected to this node
  int *subList;  // list of subd connected to this node

public:
  NodeConnect(int ns, int *l) { nsub = ns; subList = l; }
  bool operator < (const NodeConnect &n2) const {
    if(nsub < n2.nsub) return true;

    if(nsub > n2.nsub) return false;

      for(int i = 0; i < nsub; ++i)
        if(subList[i] != n2.subList[i])
          return subList[i] < n2.subList[i];
      return false;
  }

};

#endif
