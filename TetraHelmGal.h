#ifndef _TETRAHELMGAL_H_
#define _TETRAHELMGAL_H_

#include <Element.h>

class TetraHelmGal: public Element {


public:
	TetraHelmGal(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 40; }

        void binWriteNodes(BinFileHandler &);
	
};
#endif

