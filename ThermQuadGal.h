#ifndef _THERMQUADGAL_H_
#define _THERMQUADGAL_H_

#include <Element.h>

class ThermQuadGal: public Element {


public:
	ThermQuadGal(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 10; }

        void binWriteNodes(BinFileHandler &);

};
#endif

