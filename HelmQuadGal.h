#ifndef _HELMQUADGAL_H_
#define _HELMQUADGAL_H_

#include <Element.h>

class HelmQuadGal: public Element {

public:
	HelmQuadGal(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 30; }

        void binWriteNodes(BinFileHandler &);
};
#endif

