#ifndef _TETRATHERM_H_
#define _TETRATHERM_H_

#include <Element.h>

class TetraTherm: public Element {


public:
	TetraTherm(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	
  	void binWriteNodes(BinFileHandler &);
};
#endif

