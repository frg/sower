#ifndef _BIN_XPOST_
#define _BIN_XPOST_

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define FALSE 0
#define TRUE 1
#define MAXENTRY 512

typedef int BOOLEAN;

typedef int Flagfield;

enum flagword {NONOBJECT, NODESET, ELEMSET, VECTORSET, SCALARSET, CONG,
       NODE, ELEMENT, VECTOR, SCALAR,NODE_GROUP, ELEM_GROUP, DECOMP_ELEM,
       SDBOUNDARYSET, SFBOUNDARYSET, FBOUNDARYSET, FSBOUNDARYSET,
       FSBOUNDARY,TPBOUNDARYSET };
     // used in reading binary file for switch statement

enum elemtype {NONELEMENT, BAR2, QUAD4, CUBE8, TRI3, TETRA, ELEMENT16, QUAD9,
        TRI6, CUBE20, PENTA6, TETRA10, QUAD8};
     // used in reading binary elements for switch statement


//------------------------------------------------------------------------------

class XpostSet
{

public:

  XpostSet() {}
  ~XpostSet() {}

  virtual void writeHeader() {}
  virtual void writeXpostSolution(double t, double* u) {}

};

//------------------------------------------------------------------------------

class Scalarset : public XpostSet
{

  int fd;
  int nsize;
  float* tmp;
  char* scalar_type;
  char* load_name;
  char* nset_name;

public:

  Scalarset(char* name, char* st, char* ln, char* sn, int ns) { 
    fd = open(name, O_RDWR | O_CREAT | O_TRUNC, 0644);
    scalar_type = st; 
    load_name = ln; 
    nset_name = sn;
    nsize = ns;
    tmp = new float[nsize];
  }
  ~Scalarset() { close(fd); if (tmp) delete [] tmp; }

  void writeHeader() {
    Flagfield flag = SCALARSET;
    write(fd, (char *)&flag, sizeof(Flagfield));

    int namelen = strlen(scalar_type);
    write(fd, (char *)&namelen, sizeof(int));
    write(fd, scalar_type, namelen);

    namelen = strlen(load_name);
    write(fd, (char *)&namelen, sizeof(int));
    write(fd, load_name, namelen);

    namelen = strlen(nset_name);
    write(fd, (char *)&namelen, sizeof(int));
    write(fd, nset_name, namelen);

    write(fd, (char *)&nsize, sizeof(int));
  }
 
  void writeSolution(float steptime, float* sval) {
    Flagfield flag = SCALAR;
    write(fd, (char *)&flag, sizeof(Flagfield));
    write(fd, (char *)&steptime, sizeof(float));
    write(fd, (char *)sval, nsize*sizeof(float));
  }

  void writeXpostSolution(double t, double* u) {
    for (int i=0; i<nsize; ++i)
      tmp[i] = u[i];
    writeSolution(static_cast<float>(t), tmp);
  }

};

//------------------------------------------------------------------------------

class Vectorset : public XpostSet {

  int fd;
  int nsize;
  float* tmp;
  char* vector_type;
  char* load_name;
  char* nset_name;

public:

  Vectorset(char* name, char* vt, char* ln, char* sn, int ns) { 
    fd = open(name, O_RDWR | O_CREAT | O_TRUNC, 0644);
    vector_type = vt; 
    load_name = ln; 
    nset_name = sn;
    nsize = ns;
    tmp = new float[nsize*3];
  }
  ~Vectorset() { close(fd); if (tmp) delete [] tmp; }

  void writeHeader() {
    Flagfield flag = VECTORSET;
    write(fd, (char *)&flag, sizeof(Flagfield));

    int namelen = strlen(vector_type);
    write(fd, (char *)&namelen, sizeof(int));
    write(fd, vector_type, namelen);

    namelen = strlen(load_name);
    write(fd, (char *)&namelen, sizeof(int));
    write(fd, load_name, namelen);

    namelen = strlen(nset_name);
    write(fd, (char *)&namelen, sizeof(int));
    write(fd, nset_name, namelen);

    write(fd, (char *)&nsize, sizeof(int));
  }

  void writeSolution(float steptime, float* vval) {
    Flagfield flag = VECTOR;
    write(fd, (char *)&flag, sizeof(Flagfield));
    write(fd, (char *)&steptime, sizeof(float));
    write(fd, (char *)vval, nsize*3*sizeof(float));
  }

  void writeXpostSolution(double t, double* u) {
    for (int i=0; i<nsize*3; ++i)
      tmp[i] = u[i];
    writeSolution(static_cast<float>(t), tmp);
  }

};

//------------------------------------------------------------------------------

#endif
