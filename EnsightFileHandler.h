#ifndef _ENSIGHT_FILE_HANDLER_H_
#define _ENSIGHT_FILE_HANDLER_H_

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <iostream>

//------------------------------------------------------------------------------

enum fmode {ASCII, BIN};

class EnsightFileHandler {

  static const int bufflen = 80;

public:

  //typedef off_t OffType;
#if defined(__sgi) 
  typedef long long OffType; 
  inline OffType fseek(FILE *fp, OffType offset, int whence)  { return fseek64(fp, offset, whence); }
  inline OffType ftell(FILE *fp)  { return ftell64(fp); }
#elif defined(linux)
  typedef __off64_t  OffType;
  inline OffType fseek(FILE *fp, OffType offset, int whence)  { return fseeko64(fp, offset, whence); }
  inline OffType ftell(FILE *fp)  { return ftello64(fp); }
#elif defined(Darwin)
  typedef long  OffType;
#elif defined(__sun)
  typedef long OffType; 
#else
#error Update the definition of OffType for your machine
#endif

private:

  FILE  *file;
  fmode  m;
  int    part_id;

public:

  EnsightFileHandler(const char *, const char *);
  ~EnsightFileHandler();

  void write(const char *);
  void write(int *, int, int = 0);
  void write(float *, int, int = 0);

  template<class Scalar>
  void write(Scalar *, int, int = 0);

  void seek(OffType);

  OffType tell();

  void header(const char *desc1, const char *desc2 = NULL);
  void part(const char *desc = NULL);

};

//------------------------------------------------------------------------------

inline
void EnsightFileHandler::write(const char *str)
{
  char   cbuf[bufflen];
  size_t len;
  int i;

  // Most of this just to avoid garbage after the name.
  len = strlen(str);
  strncpy(cbuf, str, len < bufflen ? len : bufflen);

  for (i = len; i < bufflen; i++)
    cbuf[i] = '\0'; // pad with zeros

  switch (m) {
  case ASCII:
    fprintf(file, "%s\n", cbuf);
    break;
  case BIN:
    fwrite(cbuf, sizeof(char), bufflen, file);
    break;
  }
}

//------------------------------------------------------------------------------

inline
void EnsightFileHandler::write(int *p, int nobjs, int npl)
{

  switch (m) {
  case ASCII:
    if (npl) {
      int i, k;
      assert(npl>0);
      for (i=0; i< nobjs; i++) {
	for (k=0; k<npl; k++)
	  fprintf(file, " %d", p[i*npl+k]);
	fprintf(file, "\n");
      }
    } else {
      for (int i=0; i< nobjs; i++)
	fprintf(file, " %d", p[i]);
      fprintf(file, "\n");
    }
    break;
  case BIN:\
    if (npl==0) npl = 1; assert(npl>0);
    fwrite(p, sizeof(int), nobjs*npl, file);
    break;
  }

}

//------------------------------------------------------------------------------

inline
void EnsightFileHandler::write(float *p, int nobjs, int npl)
{

  switch (m) {
  case ASCII:
    if (npl) {
      int i, k;
      assert(npl>0);
      for (i=0; i< nobjs; i++) {
	for (k=0; k<npl; k++)
	  fprintf(file, " %e", p[i*npl+k]);
	fprintf(file, "\n");
      }
    } else {
      for (int i=0; i<nobjs; i++)
	fprintf(file, " %e", p[i]);    
      fprintf(file, "\n");
    }
    break;
  case BIN:
    if (npl==0) npl = 1; assert(npl>0);
    fwrite(p, sizeof(float), nobjs*npl, file);
    break;
  }

}

//------------------------------------------------------------------------------

template <class Scalar>
void EnsightFileHandler::write(Scalar *p, int nobjs, int npl)
{
  float *buff = new float[nobjs];

  for (int i=0; i<nobjs; i++)
    buff[i] = p[i];

  write(buff, nobjs, npl);

  delete [] buff;
}

//------------------------------------------------------------------------------
inline
void EnsightFileHandler::header(const char *desc1, const char *desc2)
{

  if (m==BIN && desc2)
    write("C Binary");  

  write(desc1);

  if (desc2) {
    write(desc2);
    write("node id ignore");
    write("element id ignore");
  }

}

//------------------------------------------------------------------------------

inline
void EnsightFileHandler::part(const char *desc)
{
  
  part_id++;
  write("part");
  write(&part_id, 1);

  if (desc)
    write(desc);

}

//------------------------------------------------------------------------------

inline
void EnsightFileHandler::seek(EnsightFileHandler::OffType size) 
{ 

  fseek(file, size, SEEK_SET);

}

//------------------------------------------------------------------------------

inline
EnsightFileHandler::OffType EnsightFileHandler::tell() 
{ 

  int pos;

  pos = ftell(file);

  return pos;

}

//------------------------------------------------------------------------------

inline
EnsightFileHandler::EnsightFileHandler(const char *name, const char *flag) :
 file(0), part_id(0)
{

  int ierr = 0;

  if (strcmp(flag, "w") == 0) {
    file = fopen(name, "w");
    m = ASCII;
    if (!file) ierr = 1;
  }
  else if (strcmp(flag, "wb") == 0) {
    file = fopen(name, "wb");
    m = BIN;
    if (!file) ierr = 1;
  }
  else {
    fprintf(stderr, "*** Error: wrong flag (%s) for \'%s\'\n", flag, name);
    exit(1);
  }

  if (ierr) {
    fprintf(stderr, "*** Error: unable to open \'%s\'\n", name);
    exit(1);
  }

}

//------------------------------------------------------------------------------

inline
EnsightFileHandler::~EnsightFileHandler() 
{ 

  fclose(file); 

}

#endif
