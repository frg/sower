#ifndef _GREEDY_H_
#define _GREEDY_H_

#include "Connectivity.h"

class GreedyDecomp {

  const char *esname;  // Name of the elementset for which this is a decomposition
  int nsub;      // Number of subdomains
  int *pele;     // pointer into eln
  int *eln;      // List of elements in each subdomain

  enum Alg { Greedy , Experimental } ;

public:

  GreedyDecomp() {}
  GreedyDecomp(const char *name, int ns, Alg = Greedy) { esname = name; nsub = ns; }
  ~GreedyDecomp(){}

  // etoe -> subdomain to subdomain
  // ntoe -> node to subdomain
  // eton -> subdomain to node
  Connectivity *cpuGreedyBuild(Connectivity *etoe, Connectivity *ntoe, 
			       Connectivity*eton, long long *sizes, int numSub);
  Connectivity *greedyBuild(Connectivity *, Connectivity *, Connectivity *);

  void outputDump(FILE *,int);
  void print();
};

#endif
