#ifndef _TRIANGLE3_H_
#define _TRIANGLE3_H_

#include <Element.h>

class Triangle3: public Element {

public:
	Triangle3(int*);

        int              numNodes();
        int*             nodes(int * = 0);
};
#endif

