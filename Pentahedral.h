#ifndef _PENTAHEDRAL_H_
#define _PENTAHEDRAL_H_

#include <Element.h>

class Pentahedral: public Element {

public:
	Pentahedral(int*);


        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 24; }
	void binWriteNodes(BinFileHandler &);
};
#endif

