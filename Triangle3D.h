//AUTHOR: Chris Saam
//FILE:	Triangle3D.h
//CLASS PROVIDED: Triangle3D (a class containing array  
//addresses of 3 Point3D types stored in an array provided 
//by the user.)
//CONSTUCTORS for the Triangle3D class:
//	Triangle3D()
//	Postcondition: the Triangle3D object has been initialized
//		with the pointer list containing -1 (non-valid addresses).
//	Triangle3D(int _n1, int _n2, int _n3)
//	Postcondition: the Triangle3D object is initialized
//		to contain the three addresses int the argument list.
//MODIFIACTION MEMBER OPERATOR for the Triangle3D class:
//  int operator[](const int position)
//	Precondition: 0<=position<=2.
//	Postcondition: the address stored at node[position] is accessed.
//INSERTION AND EXTRACTION OPERATORS for the Triangle3D class:
//	friend ostream& operator<<(ostream& os, const Triangle3D& source)
//	Postcondition: the addresses stored in source are sent to the
//		output stream with a single space between addresses and without
//		a new line character.
//	friend istream& operator>>(istream& is, Triangle3D& source)
//	Postcondition: sources addresses are read in from the input 
//		stream is and one is subtracted from each address.  The 
//		input files refer to order the Point3D objects were read in
//		beginning with one (the first Point3D object read).
//		So the subtraction of one is necessary to correspond
//		to C's array addressing convention.
//VALUE SEMANTICS for the Triangle3D class:
//  The assignment operator and copy constructor can be used and are
//  the defaults.

#ifndef _TRIANGLE3D_H_
#define _TRIANGLE3D_H_
#include<cstdlib>
#include<cassert>
#include <iomanip>
#include<iostream>
using std::ostream;
using std::istream;

class Triangle3D{
private:
  int node[3];
public:
  Triangle3D(){node[0]=node[1]=node[2]=-1;}
  Triangle3D(int _n1, int _n2, int _n3) 
	{node[0] = _n1;node[1] = _n2;node[2] = _n3; }
	int& operator[](const int position );
	friend ostream& operator<<(ostream& os, const Triangle3D& source)
	{return os<<(source.node[0]+1)<<' '<<(source.node[1]+1)<<' '
		<<(source.node[2]+1);}
	friend istream& operator>>(istream& is, Triangle3D& source);
        int *getFace() {return node;}
};  

#endif
