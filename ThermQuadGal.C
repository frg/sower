#include        <ThermQuadGal.h>



// Four Node Quad element (Galerkin)

ThermQuadGal::ThermQuadGal(int* nodenums)
{
	nn = new int[4];

	nn[0] = nodenums[0];
	nn[1] = nodenums[1];
	nn[2] = nodenums[2];
	nn[3] = nodenums[3];
}

//-------------------------------------------------

int
ThermQuadGal::numNodes()
{
 	return 4;
}
//-------------------------------------------------

int*
ThermQuadGal::nodes(int *p)
{
 	if(p == 0) p = new int[4];
 	p[0] = nn[0];
 	p[1] = nn[1];
 	p[2] = nn[2];
 	p[3] = nn[3];
	return p;
}
//-------------------------------------------------

void ThermQuadGal::binWriteNodes(BinFileHandler &outfile)  {

  int type = 10;
  outfile.write(&type, 1);
  outfile.write(nn,4);
}
