#ifndef _TRANSSPRLINK_H_
#define _TRANSSPRLINK_H_

#include <Element.h>

class TransSprlink : public Element {

public:

	TransSprlink(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 21;}

  	void binWriteNodes(BinFileHandler &);
};

#endif
