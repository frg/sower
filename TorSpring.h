#ifndef _TORSPRING_H_
#define _TORSPRING_H_

#include <Element.h>

class TorSpring : public Element {


public:

	TorSpring(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 11; }

	void binWriteNodes(BinFileHandler &);
};
#endif
