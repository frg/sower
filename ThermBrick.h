#ifndef _THERMBRICK_H_
#define _THERMBRICK_H_

#include <Element.h>

class ThermBrick: public Element {

public:
	ThermBrick(int*);


        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 52; }

   	void binWriteNodes(BinFileHandler &);
};
#endif

