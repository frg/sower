#ifndef _ELEMENT_TYPES_
#define _ELEMENT_TYPES_

#define TWONODETRUSS       1
#define FOURNODEQUAD       2
#define EULERBEAM          6
#define TIMOSHENKOBEAM     7
#define THREENODESHELL     8
#define TORSPRING          11
#define EIGHTNODEBRICK     17
#define MEMBRANE           19
#define COMPO3NODESHELL    20 
#define TRANSSPRLINK       21
#define ROTNSPRLINK        22
#define TETRAHEDRAL        23
#define PENTAHEDRAL        24
#define TENNODETETRAHEDRAL 25
#define HELMQUADGAL        30
#define HELMQUADGLS        31
#define TETRAHELMGAL       40
#define TETRAHELMGLS       41
#endif
