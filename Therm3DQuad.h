#ifndef _THERM3DQUAD_H_
#define _THERM3DQUAD_H_

#include <Element.h>

class Therm3DQuad: public Element {


public:
	Therm3DQuad(int*);

        int              numNodes();
        int*             nodes(int * = 0);
	int getType()  { return 3; }

	void binWriteNodes(BinFileHandler &);
};

#endif
