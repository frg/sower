#ifndef _HELMISOPARAMHEXA_H_
#define _HELMISOPARAMHEXA_H_
                                                                                                 
#include <Element.h>
                                                                                                 
class HelmIsoParamHexa: public Element {
                                                                                                 
        double *C;
        int nNodes;
public:
        HelmIsoParamHexa(int,int*);
                                                                                                 
        int  numNodes();
        int* nodes(int * = 0);
        int getType()  { return 95; }
                                                                                                 
        void binWriteNodes(BinFileHandler &);
                                                                                                 
};
#endif
 
