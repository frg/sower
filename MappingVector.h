#ifdef OLD_STL
#include <map.h>
#include <algo.h>
#else
#include <map>
#include <algorithm>
using namespace std;
#endif

#ifdef OLD_STL
  typedef map<int, int, less<int> > MapVec;
#else
  typedef map<int, int> MapVec;
#endif
