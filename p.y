%{
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include "aux.h"
#include "Domain.h"
#if defined(linux)
extern int  yylex(void);
extern void yyerror(const char  *);
#endif
extern StructureDomain *domain;
 int numColumns = 3;
 double amplitude = 1.0;
%}

%union
{
 int ival;
 double fval;
 char * strval;
 NumedNode nval;
 NumList nl;
 FrameData frame;
 BCond bcval;
 BCList *bclist;
 LayerData ldata;
 LayInfo *linfo;
 CoefData coefdata;
}

%token ACTUATORS AERO AEROHEAT AEROTYPE ARCLENGTH ATTRIBUTES 
%token AUGMENT AUGMENTTYPE AVERAGED 
%token AXIHDIR AXIHNEU AXINUMMODES AXINUMSLICES AXIHSOMMER AXIMPC
%token ATDDIR ATDNEU ATDDNB ATDROB ATDARB ACOUSTIC
%token BUCKLE 
%token COARSESOLVER COEF CFRAMES COLLOCATEDTYPE CONVECTION COMPOSITE CONDITION 
%token CONSTRMAT CONTROL CORNER CORNERTYPE 
%token DAMPING DblConstant DIMASS DISP DIRECT DLAMBDA DOFTYPE DP DYNAM 
%token EIGEN EFRAMES ELSCATTERER END 
%token FACE FACOUSTICS FETI FETI2TYPE FETIPREC FFP FITALG FNAME FLUX FORCE FRONTAL 
%token GEPS GLOBALTOL GRAVITY GRBM GTGSOLVER 
%token HDIRICHLET HEAT HFETI HNEUMAN HSOMMERFELD
%token IACC IDENTITY IDIS IDIS6 IntConstant INTERFACELUMPED ITEMP ITERTYPE IVEL 
%token LAYC LAYN LFACTOR LMPC LOAD LOCALSOLVER 
%token MASS MATERIALS MATSPEC MATTYPE MATUSAGE MAXITR MAXORTHO MAXVEC MECHHEAT MFTT MULTIPLIERS 
%token NEIGPA NEWMARK NewLine NL NLPREC NOCOARSE NODE NOSCATTERER 
%token NSBSPV NLTOL NUMCGM 
%token OPTIMIZATION OUTPUT OUTPUT6 
%token QSTATIC
%token PENALTY PRECNO PRECONDITIONER PRESSURE PRINTMATLAB PROJ 
%token RBMFILTER RBMSET READMODE REBUILD RENUM RENUMBERID REORTHO RESTART
%token SCALING SCALINGTYPE SENSORS SOLVERTYPE 
%token STATS STEADYSTATE STEP STRESSID SUBSPACE SURFACE 
%token TANGENT TEMP THERMLOAD THETA THRU TIME TOLEIG TOLFETI TOLJAC TOLPCG TOPFILE TOPOLOGY TRBM
%token USE USERDEFINEDISP USERDEFINEFORCE 
%token VERSION 
%token ZERO
%token HELMHOLTZ WAVENUMBER BGTL INCIDENCE EDGEWS WAVETYPE ORTHOTOL SPACEDIMENSION DPH OUTERLOOP OUTERLOOPTYPE
%token DECOMPOSE NSUBS DETER HNBO  ELHSOMMERFELD

%type <bclist>   BCDataList IDisp6 TempConvection
%type <bclist>   NeumanBC TempNeumanBC
%type <bclist>   TempDirichletBC DirichletBC
%type <bclist>   AtdDirScatterer AtdNeuScatterer PBCDataList
%type <bcval>    BC_Data PBC_Data
%type <coefdata> CoefList

%type <fval>     Float
%type <ival>     Integer 
%type <nl>       NodeNums SurfNodeNums SommNodeNums
%type <nval>     Node
%type <fval>     DblConstant
%type <ival>     IntConstant
%type <frame>    Frame

%type <ldata>	 LayData
%type <linfo>	 LaynInfo
%type <linfo>	 LaycInfo

%type <ival>     SCALINGTYPE SOLVERTYPE STRESSID SURFACE Pressure OUTERLOOPTYPE
%type <ival>     MATTYPE
%type <strval>   FNAME
%%
FinalizedData:
	All END
	 { 
           return 0;
         }
	 ;
All:
	Component
	| All Component
	;
Component:
	NodeSet 
	| DirichletBC
	{ if(domain->setDirichlet($1->n,$1->d) < 0) return -1; }
	| NeumanBC
	{ if(domain->setNeuman($1->n,$1->d) < 0) return -1; }
        | LMPConstrain
	| ElemSet
	| FrameDList
	| Attributes
	| Materials
	| Solver
	| Pressure
         {}
	| Renumbering
	| IDisp
	| Mode
	| IDisp6
	 {}
	| IVel
	| IAcc
	| ITemp
	| SensorLocations
	| ActuatorLocations
	| UsddLocations
	| UsdfLocations
        | DynInfo
	| OutInfo
        | NLInfo
	| DiscrMasses
	| Composites
	| Cframes
	| MFTTInfo
	| RbmTolerance
        | ToleranceInfo
        | Gravity
	| RbmFilterInfo
        | Restart
	| LoadCInfo
	| UseCInfo
        | Control
        | MatUsage
        | MatSpec
        | Optimization
        | OrthoInfo
        | NewtonInfo
	| AeroInfo
        | AeroHeatInfo
        | TempLoadInfo
        | MechHeatInfo
	| CondInfo
	| MassInfo
	| TempDirichletBC
	{ if(domain->setDirichlet($1->n,$1->d) < 0) return -1; }
        | TempNeumanBC
	{ if(domain->setNeuman($1->n,$1->d) < 0) return -1; }
        | TempConvection
	{ if(domain->setConvBC($1->n,$1->d) < 0) return -1; }
	| TopInfo
        | OldHelmInfo
	| HelmInfo
        | HelmHoltzBC
        | ComplexNeumanBC
        | ComplexDirichletBC
        | EleHelmHoltzBC
        | AtdDirScatterer
        | AtdNeuScatterer
        | AtdArbScatterer
        | AtdNeumScatterer
        | AtdRobinScatterer
        | AxiHDir
        | AxiHNeu
        | AxiNumModes
        | AxiNumSlices
        | AxiHSommer
        | AxiMPC
        | EleScatterer
        | NeumScatterer
        | FarFieldPattern
        | NodeScatterer
        | Decompose
	;
Decompose :
       DECOMPOSE NewLine
       | Decompose NSUBS Integer NewLine
       | Decompose DETER NewLine
       ;
MFTTInfo:
	MFTT NewLine
	| MFTTInfo Float Float NewLine
	;
Composites:
	COMPOSITE NewLine
	| Composites CoefInfo
	| Composites LaycInfo
	| Composites LaynInfo
	;
Cframes:
	CFRAMES NewLine
	| Cframes Frame
	{ domain->addCFrame($2.num,$2.d); }
	;
CoefInfo:
	COEF Integer NewLine CoefList
	{ domain->addCoefInfo($2-1,$4); }
	;
CoefList:
	Integer Integer Float NewLine
	{ $$.zero(); $$.setCoef($1-1,$2-1,$3); }
	| CoefList Integer Integer Float NewLine
	{ $$.setCoef($2-1,$3-1,$4); }
	;
LaycInfo:
	LAYC Integer NewLine
        { $$ = new LayInfo(0); domain->addLay($2-1,$$); }
	| LaycInfo LayData
	{ $1->add($2.lnum,$2.d); }
	;
LaynInfo:
	LAYN Integer NewLine 
	{ $$ = new LayInfo(1); domain->addLay($2-1,$$); }
	| LaynInfo LayData
	{ $1->add($2.lnum,$2.d); }
	;
LayData:
	Integer Float Float Float Float Float Float Float Float Float NewLine
	{ $$.lnum = $1-1;
          $$.d[0] = $2; $$.d[1] = $3; $$.d[2] = $4;
          $$.d[3] = $5; $$.d[4] = $6; $$.d[5] = $7;
          $$.d[6] = $8; $$.d[7] = $9; $$.d[8] = $10; }
	;
DiscrMasses:
	DIMASS NewLine
	| DiscrMasses Integer Integer Float NewLine
	;
Gravity:
	GRAVITY NewLine
	| Gravity Float Float Float NewLine
	;
Restart:
	RESTART NewLine 
	| Restart FNAME FNAME NewLine
        | Restart FNAME Integer NewLine
	;
LoadCInfo:
	LOAD FNAME NewLine
	;
UseCInfo:
	USE FNAME NewLine
	;
SensorLocations:
        SENSORS NewLine BCDataList
	;
ActuatorLocations:
        ACTUATORS NewLine BCDataList
	;
UsdfLocations:
	USERDEFINEFORCE NewLine BCDataList
	;
UsddLocations:
	USERDEFINEDISP NewLine BCDataList
	;
OutInfo:
	OUTPUT NewLine
	| OUTPUT6 NewLine
	| OutInfo STRESSID FNAME Integer DOFTYPE NewLine
        | OutInfo STRESSID FNAME Integer NewLine
	| OutInfo STRESSID Integer Integer FNAME Integer NewLine
        | OutInfo STRESSID FNAME Integer Integer NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer Integer NewLine
        | OutInfo STRESSID FNAME Integer AVERAGED NewLine
        | OutInfo STRESSID FNAME Integer AVERAGED SURFACE NewLine
	| OutInfo STRESSID FNAME Integer SURFACE NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer AVERAGED NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer AVERAGED SURFACE NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer SURFACE NewLine
        | OutInfo STRESSID FNAME Integer Integer SURFACE NewLine
        | OutInfo STRESSID Integer Integer FNAME Integer Integer SURFACE NewLine
	;
DynInfo:
        DynamInfo
	| EIGEN NewLine
	| EIGEN Integer NewLine
	| NEIGPA Integer NewLine
	| SUBSPACE NewLine
        | SUBSPACE Integer Float Float NewLine
	| NSBSPV Integer NewLine
	| TOLEIG Float NewLine
	| TOLJAC Float NewLine
	;
MassInfo:
	MASS NewLine
	;
CondInfo:
	CONDITION NewLine Float NewLine
	| CONDITION NewLine
	;
TopInfo:
	TOPFILE NewLine
	;
DynamInfo:
        DYNAM NewLine 
	| DynamInfo TimeIntegration
        | DynamInfo TimeInfo
        | DynamInfo DampInfo
        | DynamInfo TempInfo
        | DynamInfo SteadystateInfo
	;
TimeIntegration:
        NEWMARK NewLine
	| NEWMARK Float Float NewLine    	
        | QSTATIC Float Float Integer NewLine
        | ACOUSTIC NewLine
        | ACOUSTIC Float Float NewLine
        | ACOUSTIC Float Float Float Float NewLine
        | ACOUSTIC Float NewLine
	;
AeroInfo:
        AERO AEROTYPE NewLine
	| AERO NewLine AEROTYPE NewLine
	| AERO NewLine AEROTYPE Float Float NewLine
	| AeroInfo COLLOCATEDTYPE NewLine
	;
AeroHeatInfo:
        AEROHEAT NewLine AEROTYPE Float Float NewLine
	;
TempLoadInfo:
        THERMLOAD NewLine AEROTYPE Float Float NewLine
	;
MechHeatInfo:
        MECHHEAT NewLine AEROTYPE Float Float NewLine
	;
RbmTolerance:
        TRBM NewLine Float NewLine
        | TRBM NewLine Float Float NewLine
	;
ToleranceInfo:
        GRBM NewLine Float Float NewLine
        | GRBM NewLine Float NewLine
	;
RbmFilterInfo:
	RBMFILTER NewLine
	;
TimeInfo:
	TIME Float Float Float NewLine
	;
DampInfo:
	DAMPING Float Float NewLine
	;
TempInfo:
        HEAT Float NewLine
	;
SteadystateInfo:
        STEADYSTATE Float Integer Integer NewLine
	;
ComplexDirichletBC:
	HDIRICHLET NewLine ComplexBCDataList
	;
DirichletBC:
        DISP NewLine BCDataList
	{ $$ = $3; }
	;
TempDirichletBC:
        TEMP NewLine
	{ $$ = new BCList; }
        | TempDirichletBC Integer Float NewLine
	{ $$ = $1; BCond bc; bc.nnum = $2-1; bc.dofnum = 6;
          bc.val = $3; $$->add(bc); }
	;
TempNeumanBC:
        FLUX NewLine
	{ $$ = new BCList; }
        | TempNeumanBC Integer Float NewLine
	{ $$ = $1; BCond bc; bc.nnum = $2-1; bc.dofnum = 6;
          bc.val = $3; $$->add(bc); }
	;
TempConvection:
        CONVECTION NewLine
	{ $$ = new BCList;
         fprintf(stderr," *** WARNING: Convection data was found.\n"
                     "Turning on RCFEM back compatibility mode.\n"
                     "Internal convective flux computation is turned off\n");
        }
        | TempConvection Integer Float Float Float NewLine
	{ $$ = $1; BCond bc; bc.nnum = $2-1; bc.dofnum = 6;
          bc.val = $3*$4*$5; $$->add(bc); }
	;
HelmHoltzBC:
        HSOMMERFELD NewLine SommerfeldBCDataList
	;
SommerfeldBCDataList:
        SommerfeldBC_Data
        | SommerfeldBCDataList SommerfeldBC_Data
	;
SommerfeldBC_Data:
        Integer Integer Float NewLine
	{}
        | Integer Integer Integer Float NewLine
	{}
	;
AxiHDir:
        AXIHDIR NewLine
        | AxiHDir AxiHD
	;
AxiHD:
        Integer Float Float NewLine
	{}
        | Integer Float Float Float Float Float NewLine
	{}
	;
AxiHNeu:
        AXIHNEU NewLine
        | AxiHNeu AxiHN
	;
AxiHN:
        Integer Integer Float Float NewLine
	{}
        | Integer Integer Float Float Float Float Float NewLine
	{}
	;
AxiNumModes:
        AXINUMMODES Integer NewLine
	;
AxiNumSlices:
        AXINUMSLICES Integer NewLine
	;
AxiHSommer:
        AXIHSOMMER NewLine AxiHSDataList
	;
AxiHSDataList:
        AxiHSData
        | AxiHSDataList AxiHSData
	;
AxiHSData:
        Integer Integer NewLine
	{}
        | Integer Integer Integer NewLine
	{}
	;
AxiMPC:
        AXIMPC NewLine
        | AxiMPC AxiLmpc
	;
AxiLmpc:
        Integer Float Float Integer NewLine
	{}
        | Integer Float Float Integer Float Float NewLine
	{}
        | Integer Float Float Integer Float Float Float Float Float NewLine
	{}
	;
EleScatterer:
        ELSCATTERER NewLine
        | EleScatterer ScatterElement
	;
ScatterElement:
        Integer Integer Float NodeNums NewLine
	{}
	;
NeumScatterer:
        HNBO NewLine NeumElement
        | NeumScatterer NeumElement
        ;
NeumElement:
        Integer Integer SommNodeNums NewLine
        ;
EleHelmHoltzBC:
        ELHSOMMERFELD NewLine SommerElement
        | EleHelmHoltzBC SommerElement
        ;
SommerElement:
        Integer Integer SommNodeNums NewLine
        ;
SommNodeNums:
        Integer
        { }
        | SommNodeNums Integer
        ;
NodeScatterer:
        NOSCATTERER NewLine
        | NodeScatterer Integer NewLine
	;
FarFieldPattern:
        FFP Integer NewLine
	;
Mode:
	READMODE FNAME NewLine
	;
IDisp:
	IDIS NewLine BCDataList
	{ if(domain->setIDis($3->n,$3->d) < 0) return -1; }
	| IDIS ZERO NewLine
	{}
	;
IDisp6:
	IDIS6 Float NewLine
	{ $$ = new BCList; amplitude = $2;  }
	| IDIS6 NewLine
	{ $$ = new BCList; amplitude = 1.0; }
	| IDisp6 Integer Float Float Float Float Float Float NewLine
	{ BCond bc; /* add 6 boundary conditions */
          bc.nnum = $2-1; bc.dofnum = 0; bc.val = amplitude*$3; $$->add(bc);
                          bc.dofnum = 1; bc.val = amplitude*$4; $$->add(bc);
                          bc.dofnum = 2; bc.val = amplitude*$5; $$->add(bc);
                          bc.dofnum = 3; bc.val = amplitude*$6; $$->add(bc);
                          bc.dofnum = 4; bc.val = amplitude*$7; $$->add(bc);
                          bc.dofnum = 5; bc.val = amplitude*$8; $$->add(bc);
          domain->setIDis6($$->n, $$->d);
        }
	| GEPS NewLine
	{ fprintf(stderr," ... Geometric Pre-Stress Effects   ... \n"); }
	| BUCKLE NewLine
	{}
	;
IVel:
        IVEL NewLine BCDataList
	;
IAcc:
        IACC NewLine BCDataList
	;
ITemp:
        ITEMP NewLine BCDataList
	;
NeumanBC:
	FORCE NewLine BCDataList
	{ $$ = $3; }
	;
BCDataList:
	BC_Data
	{ $$ = new BCList; $$->add($1); }
	| BCDataList BC_Data
	{ $$ = $1; $$->add($2); }
        | Integer THRU Integer Integer Float NewLine
        { $$ = new BCList; for(int i=$1; i<=$3; ++i) { BCond bc; bc.setData(i-1, $4-1, $5); $$->add(bc); }} 
        | BCDataList Integer THRU Integer Integer Float NewLine
        { $$ = $1; for(int i=$2; i<=$4; ++i) { BCond bc; bc.setData(i-1, $5-1, $6); $$->add(bc); }} 
        | Integer THRU Integer STEP Integer Integer Float NewLine
        { $$ = new BCList; for(int i=$1; i<=$3; i+=$5) { BCond bc; bc.setData(i-1, $6-1, $7); $$->add(bc); } }
        | BCDataList Integer THRU Integer STEP Integer Integer Float NewLine
        { $$ = $1; for(int i=$2; i<=$4; i+=$6) { BCond bc; bc.setData(i-1, $7-1, $8); $$->add(bc); } }
	;
LMPConstrain:
        LMPC NewLine
        | LMPC NewLine MPCList
	;
MPCList:
        MPCHeader MPCLine
        | MPCList MPCLine
        | MPCList MPCHeader MPCLine
	;
MPCHeader:
        Integer Float NewLine
	{}
        | Integer NewLine
	{}
	;
MPCLine:
        Integer Integer Float NewLine
	{}
	;
ComplexNeumanBC:
	HNEUMAN NewLine ComplexBCDataList
	;
ComplexBCDataList:
	ComplexBC_Data
        | ComplexBCDataList ComplexBC_Data
	;
AtdDirScatterer:
        ATDDIR NewLine PBCDataList
        { /* $$ = $3; */ }
        ;
AtdNeuScatterer:
        ATDNEU NewLine PBCDataList
        { /* $$ = $3; */ }
        ;
AtdArbScatterer:
        ATDARB Float NewLine SurfaceElement
        { }
        | AtdArbScatterer SurfaceElement
                ;
AtdNeumScatterer:
        ATDDNB Float NewLine SurfaceElement
        { }
        | AtdNeumScatterer SurfaceElement
        ;
AtdRobinScatterer:
        ATDROB Float Float Float NewLine SurfaceElement
        { } 
        | AtdRobinScatterer SurfaceElement
        ;
SurfaceElement:
        Integer Integer SurfNodeNums NewLine
        ;
SurfNodeNums:
        Integer
        { /* $$.num = 1; $$.nd[0] = $1-1; */ }
        | SurfNodeNums Integer
        { /* if($$.num == 64) return -1;
          $$.nd[$$.num] = $2-1; $$.num++;*/ }
        ;
PBCDataList:
        PBC_Data
        { $$ = new BCList; $$->add($1); }
        | PBCDataList PBC_Data
        { $$ = $1; $$->add($2); }
        ;
Materials:
	MATERIALS NewLine MatData
	| Materials MatData
	;
MatData:
	Integer Float Float Float Float Float Float Float Float Float Float Float Float Float Float NewLine
 	{ int *tmp = new int[256]; 
	  StructProp sp;
          sp.A = $2;  sp.E = $3;  sp.nu  = $4;  sp.rho = $5;
          sp.c = $6;  sp.k = $7;  sp.eh  = $8;  sp.P   = $9;  sp.Ta  = $10;
          sp.Q = $11; sp.W = $12; sp.Ixx = $13; sp.Iyy = $14; sp.Izz = $15;
          domain->addMat( $1-1, sp );
        }
        | Integer Float Float Float Float Float Float Float NewLine
	{ StructProp sp;
          sp.A = $2; sp.E = $3; sp.nu = $4; sp.rho = $5;
          sp.c = $6; sp.k = $7; sp.eh = $8;
          domain->addMat( $1-1, sp ); }
        | Integer CONSTRMAT NewLine
        | Integer CONSTRMAT ConstraintOptionsData NewLine
        | Integer MASS Float Float Float Float Float Float Float Float Float Float NewLine
        | Integer CONSTRMAT MASS Float NewLine
        | Integer CONSTRMAT ConstraintOptionsData MASS Float NewLine
        | Integer CONSTRMAT MASS Float Float NewLine
        | Integer CONSTRMAT ConstraintOptionsData MASS Float Float NewLine
        | Integer CONSTRMAT Integer Float Float Float Float Float Float Integer NewLine
	;
ConstraintOptionsData:
        MULTIPLIERS
        | PENALTY Float
        ;
ElemSet:
	TOPOLOGY NewLine Element
	| ElemSet Element
	;
NodeSet:
	NODE NewLine Node
	{ domain->addNode($3.num, $3.xyz); }
	| NodeSet Node
	{ domain->addNode($2.num, $2.xyz); }
	;
Node:
	Integer Float Float Float NewLine
	{ $$.num = $1-1; $$.xyz[0] = $2; $$.xyz[1] = $3;  $$.xyz[2] = $4; }
	| Integer Float Float NewLine
	{ $$.num = $1-1; $$.xyz[0] = $2; $$.xyz[1] = $3;  $$.xyz[2] = 0.0; }
	| Integer Float NewLine
	{ $$.num = $1-1; $$.xyz[0] = $2; $$.xyz[1] = 0.0; $$.xyz[2] = 0.0; }
	;
Element:
	Integer Integer NodeNums NewLine
	{ domain->elemAdd($1-1, $2, $3.num, $3.nd); }
	;
NodeNums:
	Integer
	{ $$.num = 1; $$.nd[0] = $1-1; }
	| NodeNums Integer
	{ if($$.num == 256) return -1; 
          $$.nd[$$.num] = $2-1; $$.num++; }
	;
BC_Data:
	Integer Integer Float NewLine
	{$$.nnum = $1-1; $$.dofnum = $2-1; $$.val = $3;}
	| Integer Integer NewLine
	{ $$.nnum = $1-1; $$.dofnum = $2-1; $$.val = 0.0; }
	;
PBC_Data:
        Integer Float NewLine
        { $$.nnum = $1-1; $$.dofnum = 7; $$.val = $2; }
        ;
ComplexBC_Data:
	Integer Integer Float Float NewLine
	{}
	| Integer Integer Float NewLine
	{}
	;
FrameDList:
	EFRAMES NewLine Frame
	{ domain->setFrame($3.num,$3.d); }
        | FrameDList Frame
        { domain->setFrame($2.num,$2.d); }
	;
Frame:
	Integer Float Float Float Float Float Float Float Float Float NewLine
	{ $$.num = $1-1;
            $$.d[0] = $2; $$.d[1] = $3; $$.d[2] = $4;
            $$.d[3] = $5; $$.d[4] = $6; $$.d[5] = $7;
            $$.d[6] = $8; $$.d[7] = $9; $$.d[8] = $10; }
	;
Attributes: ATTRIBUTES NewLine
	| Attributes Integer Integer Integer Integer NewLine
        { domain->setAttrib($2-1,$3-1,$4-1,$5-1); }
	| Attributes Integer Integer NewLine
        { domain->setAttrib($2-1,$3-1); }
        | Attributes Integer Integer Integer THETA Float NewLine
        { domain->setAttrib($2-1,$3-1,$4-1,-1,$6); }
	| Attributes Integer Integer IDENTITY NewLine
        { int i;
          for(i=$2-1; i<$3; ++i)
            domain->setAttrib(i,i);
        }
	| Attributes Integer Integer Integer NewLine
        { int i;
          for(i=$2; i<$3+1; ++i)
            domain->setAttrib(i-1,$4-1);
        }
	| Attributes Integer Integer Integer Integer Integer NewLine
	{ int i;
          for(i=$2; i<$3+1; ++i)
            domain->setAttrib(i-1, $4-1, $5-1, $6-1);
        }
        | Attributes Integer Integer Integer Integer THETA Float NewLine
        { int i;
          for(i=$2; i<$3+1; ++i)
            domain->setAttrib(i-1, $4-1, $5-1, -1, $7);
        }
	;
Pressure:
	PRESSURE NewLine
	{ $$ = 0; }
	| Pressure Integer Float NewLine
	{ $$ = 0; }
	| Pressure Integer Integer Float NewLine
	{ $$ = 0; }
        /* New option to set element pressure using "FACE" keyword */
        | Pressure Integer FACE Integer Float NewLine
        { $$ = 0; }
        | Pressure Integer Integer FACE Integer Float NewLine
        { $$ = 0; }
	;
Solver:
	STATS NewLine DIRECT NewLine
	| STATS NewLine DIRECT Integer NewLine
	| STATS NewLine SOLVERTYPE NewLine
	| STATS NewLine ITERTYPE Integer Float Integer NewLine
	| STATS NewLine ITERTYPE Integer Float Integer Integer NewLine
        | STATS NewLine ITERTYPE NewLine PRECNO Integer NewLine TOLPCG Float NewLine MAXITR Integer NewLine
        | STATS NewLine FETI Integer Float NewLine
        | STATS NewLine FETI Integer NewLine
	| STATS NewLine FETI DP NewLine
        | STATS NewLine FETI DPH NewLine
        | STATS NewLine FETI Integer FETI2TYPE NewLine
        | STATS NewLine FETI Integer Float Integer NewLine
        | FETI Integer Float Integer NewLine
	| STATS NewLine FETI NewLine
	| FETI NewLine
	| FETI Integer NewLine
	| FETI DP NewLine
        | FETI DPH NewLine
	| FETI Integer FETI2TYPE NewLine
	| MAXITR Integer NewLine 
        | FETIPREC NewLine
	| PRECNO FETIPREC NewLine
        | PRECNO Integer NewLine
	| TOLFETI Float NewLine
	| MAXORTHO Integer NewLine
	| NOCOARSE NewLine
	| PROJ Integer NewLine
	| SCALING Integer NewLine
	| SCALING SCALINGTYPE NewLine
	| CORNER CORNERTYPE NewLine
        | AUGMENT AUGMENTTYPE NewLine
        | AUGMENT AUGMENTTYPE RBMSET NewLine
        | AUGMENT EDGEWS Integer NewLine
        | AUGMENT EDGEWS WAVETYPE Integer NewLine
        | ORTHOTOL Float NewLine
        | ORTHOTOL Float Float NewLine
        | SPACEDIMENSION Integer NewLine
        | OUTERLOOP OUTERLOOPTYPE NewLine
	| GLOBALTOL Float NewLine
	| PRINTMATLAB NewLine
	| LOCALSOLVER SOLVERTYPE NewLine
	| COARSESOLVER SOLVERTYPE NewLine
	| VERSION Integer NewLine
	| GTGSOLVER NewLine
        | ITERTYPE NewLine
        | FRONTAL NewLine
        | NLPREC NewLine
	| NLPREC Integer NewLine
	| HFETI Integer Float NewLine
        | NUMCGM Integer NewLine
        | INTERFACELUMPED NewLine
	;
OldHelmInfo:
        FACOUSTICS NewLine FAcousticData
	;
FAcousticData:
        Float NewLine
	{}
	;
HelmInfo:
        HELMHOLTZ NewLine
        | WAVENUMBER Float NewLine
        | WAVENUMBER Float Float Float NewLine
        | BGTL Integer NewLine
        | BGTL Integer Float NewLine
        | BGTL Integer Float Float NewLine
        | INCIDENCE Integer NewLine IncidenceList
        ;
IncidenceList:
        IncidenceVector
        | IncidenceList IncidenceVector
        ;
IncidenceVector:
        Float Float Float NewLine
        ;
NLInfo:
        NL NewLine MAXITR Integer NewLine NLTOL Float NewLine
	| NL Integer NewLine MAXITR Integer NewLine NLTOL Float NewLine MAXVEC Integer NewLine DLAMBDA Float NewLine LFACTOR Float NewLine
	| NL NewLine ARCLENGTH NewLine MAXITR Integer NewLine NLTOL Float NewLine MAXVEC Integer NewLine DLAMBDA Float NewLine LFACTOR Float NewLine
	| NL NewLine MAXITR Integer NewLine NLTOL Float NewLine MAXVEC Integer NewLine DLAMBDA Float NewLine LFACTOR Float NewLine
        | NLInfo TimeInfo
	| DLAMBDA Float Float NewLine
	;
NewtonInfo:
        REBUILD Integer NewLine
	| REBUILD Integer Integer NewLine
	| REBUILD NewLine TANGENT Integer NewLine
	| REBUILD NewLine TANGENT Integer NewLine PRECONDITIONER Integer NewLine
	| FITALG Integer NewLine
	| FITALG Integer Integer NewLine
	| NLTOL Float NewLine
        | NLTOL Float Float NewLine
	;
OrthoInfo:
	REORTHO NewLine
	;
Control:
	CONTROL NewLine
        | Control FNAME NewLine Integer NewLine FNAME NewLine FNAME NewLine
	;
MatUsage:
        MATUSAGE NewLine
        | MatUsage Integer Integer NewLine
        | MatUsage Integer Integer Integer NewLine
        ;
MatSpec:
        MATSPEC NewLine
        | MatSpec Integer MATTYPE Float NewLine
        | MatSpec Integer MATTYPE Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float Float Float Float Float NewLine
        | MatSpec Integer MATTYPE Float Float Float Float Float Float Float Float Float Float Float Float Float NewLine
        ;
Optimization:
	OPTIMIZATION FNAME NewLine
        | Optimization NewLine FNAME NewLine
	;
Renumbering:
	RENUM NewLine RENUMBERID NewLine
	;
Integer:
	IntConstant
	{ $$ = $1; }
	;
Float:
	IntConstant
	{ $$ = $1; }
	| DblConstant
	{ $$ = $1; }
	;
%%
