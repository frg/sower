#ifndef _DECOMP_
#define _DECOMP_
#include"Point3D.h"
#include"Tetrahedron3D.h"
#include"Triangle3D.h"
#include"Connectivity.h"
#include"aux.h"
//extern int dimension;

class Decomp{
public:
	Decomp(){};
	//virtual void readDomain(const char* name, int)=0;

	virtual void getDecomp(const char* name, int)=0;
        //virtual void getMatchInfo(const char* name)=0;
	virtual void getDomainConnect()=0;
        //virtual void makeSubD(int noOverlap)=0;
        virtual int getNumSub()=0;
        //virtual int **getConnectedDomain    ()=0;
        //virtual int  *getNConnect           ()=0;
        virtual Connectivity *getSubToNode  ()=0;
        //virtual Connectivity *getSubToSub   ()=0;
        //virtual Connectivity *getNodeToSub  ()=0;
        //virtual Connectivity *getSubToTet   ()=0;
        //virtual Connectivity *getTetToNode  ()=0;
        //virtual Connectivity *getSubToTri   ()=0;
        //virtual Connectivity **getInterfNode()=0;
        //virtual Domain  *getDomain     ()=0;
        //virtual void print()=0;

};
#endif
